# Copyright (C) 2023 Ola Benderius

# Builder
FROM alpine:edge as builder
RUN apk update && \
    apk --no-cache add \
        linux-headers \
        openssl-dev \
        cmake \
        g++ \
        make

ADD . /opt/sources
WORKDIR /opt/sources
RUN mkdir build && \
    cd build && \
    cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/tmp/dest .. && \
    make && make install


# Runtime
FROM alpine:edge
RUN apk update && \
    apk --no-cache add \
        bash \
        openssl \
        git \
        curl

WORKDIR /root
COPY --from=builder /tmp/dest /usr
COPY run-auto.sh /usr/bin
ENTRYPOINT ["/usr/bin/run-auto.sh"]
