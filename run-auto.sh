#!/bin/bash

if [ "$#" -lt 5 ]; then
  echo "Use as: $0 <PORT> <CERT> <KEY> <GIT_URL> <GIT_BRANCH> [TINYH2_PARAM]"
  exit
fi

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

git clone -b $5 $4
cd figa

sleep 5

#gdb -ex r -q --args /usr/bin/figa ${6} --crt ${2} --key ${3} --port ${1} --src ./web &
/usr/bin/figa ${6} --crt ${2} --key ${3} --port ${1} --src ./web &

while /bin/true; do
  git pull
  sleep 10
done

