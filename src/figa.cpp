/*
 * Copyright (C) 2023 Ola Benderius
 */

#include <algorithm>
#include <fstream>

#include <filesystem>

#include "tinyh2.hpp"

#include "cppty.hpp"

bool hasArg(int32_t argc, char **argv, std::string const &arg)
{
  char **end = argv + argc;
  return std::find(argv, end, arg) != end;
}

std::string getArg(int32_t argc, char **argv, std::string const &arg)
{
  char **end = argv + argc;
  char **itr = std::find(argv, end, arg);
  if (itr != end && ++itr != end) {
    return std::string(*itr);
  }
  return 0;
}

std::string getTimestamp()
{
  std::stringstream ss;
  std::time_t t = std::time(nullptr);
  ss << std::put_time(std::gmtime(&t), "%a, %d %b %Y %T %Z");
  return ss.str();
}

int32_t main(int32_t argc, char **argv)
{
  if (!hasArg(argc, argv, "--port") || !hasArg(argc, argv, "--key") ||
      !hasArg(argc, argv, "--crt") || !hasArg(argc, argv, "--src")) {
    std::cerr
        << "Run as: figa --crt file.crt --key file.key "
        << "--port 443 --src folder [--out folder (default: ./site)] [--data "
           "folder (default: ./data)]"
        << "[--update-rate seconds (default: 5.0)] [--quiet|--verbose|--debug]"
        << std::endl;
    return 0;
  }

  std::string const key_file = getArg(argc, argv, "--key");
  std::string const cert_file = getArg(argc, argv, "--crt");
  uint32_t const port = std::stoi(getArg(argc, argv, "--port"));
  uint32_t const worker_count = 1;

  tinyh2::Verbosity verbosity = tinyh2::Verbosity::ON_ERROR;
  verbosity =
      hasArg(argc, argv, "--quiet") ? tinyh2::Verbosity::NONE : verbosity;
  verbosity =
      hasArg(argc, argv, "--verbose") ? tinyh2::Verbosity::INFO : verbosity;
  verbosity =
      hasArg(argc, argv, "--debug") ? tinyh2::Verbosity::DEBUG : verbosity;

  std::string const src_path = getArg(argc, argv, "--src");
  std::string const web_path =
      hasArg(argc, argv, "--out") ? getArg(argc, argv, "--out") : "./site";
  std::string const data_path =
      hasArg(argc, argv, "--data") ? getArg(argc, argv, "--data") : "./data";
  std::filesystem::create_directories(web_path);

  float const update_rate = hasArg(argc, argv, "--update-rate")
      ? std::stof(getArg(argc, argv, "--update-rate"))
      : 5.0f;

  bool beta = hasArg(argc, argv, "--beta");

  bool do_transform = true;


  std::filesystem::create_directories(data_path);
  std::mutex data_mutex;
  std::ofstream reg_data(data_path + "/registration.db", std::ios::app);
  std::ofstream access_data(data_path + "/access.db", std::ios::app);


  auto runner = [&do_transform, &src_path, &web_path, &update_rate]() {
    do {
      cppty::transform(src_path, web_path);
      std::this_thread::sleep_for(
          std::chrono::milliseconds(static_cast<uint32_t>(update_rate * 1000)));
    } while (do_transform && update_rate > 0.0f);
  };
  std::thread transform_thread(runner);

  auto prepare_incoming_delegate{
      [](uint32_t length) -> std::shared_ptr<std::string> {
    return std::shared_ptr<std::string>(new std::string(length, ' '));
  }};

  auto append_incoming_delegate{
      [](std::shared_ptr<std::string> data_in, std::string const &data,
          uint32_t current_pos) {
    memcpy(data_in->data() + current_pos, data.data(), data.length());
  }};

  auto request_delegate{
      [&web_path, &data_mutex, &reg_data, &access_data, &beta](
          std::map<std::string, std::string> fields,
          std::shared_ptr<std::string> data_in, std::string const &client,
          std::function<void(
              std::string, uint32_t, std::shared_ptr<std::string const>)>
              send_response,
          std::function<void(std::string, void *, std::string const &)>) {
    std::string method = fields[":method"];
    std::string path = fields[":path"];

    {
      std::lock_guard<std::mutex> const lock(data_mutex);
      access_data << getTimestamp() << ": from " << client << " request "
                  << path << " (" << method << ")" << std::endl;
    }
    /*
    std::cout << method << " : " << path << std::endl;

    if (data_in != nullptr) {
      std::cout << " .. " << *data_in << std::endl;
    }
    */

    // Note: Url query fields are discareded by getUrlPath.
    std::string const file =
        (path != "/") ? tinyh2::getUrlPath(path) : std::string("/index.html");

    std::filesystem::path p{web_path + file};

    std::string content_type;
    std::string content;
    uint32_t status_code;

    if (file == "/thanks.html" && method == "POST") {
      if (data_in != nullptr) {
        std::lock_guard<std::mutex> const lock(data_mutex);
        reg_data << getTimestamp() << ": " << *data_in << std::endl;
      }
    }

    if (file == "/robots.txt" && beta) {
      status_code = 200;
      content_type = "text/plain";
      content = "User-agent: *\nDisallow: /";
    } else if (!std::filesystem::exists(p)) {
      status_code = 404;
      content_type = "text/html; charset=UTF-8";
      content = "<html><head><title>404: Not Found"
                "</title></head><body><h1>404: Not Found</h1><hr><address>"
                "tinyh2, The header-only HTTP/2 server</address></body>"
                "</html>";
    } else {
      status_code = 200;
      std::string const extension = p.extension();
      if (extension == ".html") {
        content_type = "text/html";
      } else if (extension == ".css") {
        content_type = "text/css";
      } else if (extension == ".js") {
        content_type = "text/javascript";
      } else if (extension == ".json") {
        content_type = "application/json";
      } else if (extension == ".gif") {
        content_type = "image/gif";
      } else if (extension == ".png") {
        content_type = "image/png";
      } else if (extension == ".jpeg" || extension == ".jpg") {
        content_type = "image/jpeg";
      } else if (extension == ".mp4") {
        content_type = "video/mp4";
      } else if (extension == ".otf") {
        content_type = "application/x-font-opentype";
      } else if (extension == ".eot") {
        content_type = "application/vnd.ms-fontobject";
      } else if (extension == ".svg") {
        content_type = "image/svg+xml";
      } else if (extension == ".ttf") {
        content_type = "application/x-font-ttf";
      } else if (extension == ".woff") {
        content_type = "application/font-woff";
      } else if (extension == ".woff2") {
        content_type = "application/font-woff2";
      } else {
        content_type = "text/plain";
      }

      std::ifstream ifs(p.string());
      std::stringstream ss;
      ss << ifs.rdbuf();
      content = ss.str();
    }

    std::shared_ptr<std::string const> content_p(new std::string(content));
    send_response(content_type, status_code, content_p);
  }};

  bool do_support_sse = false;

  tinyh2::Server server(verbosity);
  server.run(key_file, cert_file, port, do_support_sse, worker_count,
      prepare_incoming_delegate, append_incoming_delegate, request_delegate);

  do_transform = false;
  transform_thread.join();

  return 0;
}
