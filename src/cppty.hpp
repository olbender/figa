/*
 * Copyright (C) 2022 Ola Benderius
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPPTY
#define CPPTY

#include <fstream>
#include <sstream>

#include "mustache.hpp"
#include "json.hpp"

namespace cppty {

std::string ltrim(std::string s) noexcept
{
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
    return !std::isspace(ch);
  }));
  return s;
}

std::string rtrim(std::string s) noexcept
{
  s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
    return !std::isspace(ch);
  }).base(), s.end());
  return s;
}

std::string trim(std::string s) noexcept
{
  return ltrim(rtrim(s));
}

inline std::vector<std::string> split(std::string const &str,
    char const &delimiter) noexcept {
  std::vector<std::string> parts;
  std::string::size_type prev{0};
  for (std::string::size_type i{str.find_first_of(delimiter, prev)};
       i != std::string::npos;
       prev = i + 1, i = str.find_first_of(delimiter, prev)) {
    if (i != prev) {
      parts.emplace_back(str.substr(prev, i - prev));
    } else {
      parts.emplace_back("");
    }
  }
  if (prev < str.size()) {
    parts.emplace_back(str.substr(prev, str.size() - prev));
  } else if (prev > 0) {
    parts.emplace_back("");
  }
  return parts;
}

inline std::string idfy(std::string const &str) noexcept {
  std::string str_id(str.size(), '-');
  for (size_t i{0}; i < str.size(); i++) {
    if (str[i] >= 'A' && str[i] <= 'Z') {
      str_id[i] = str[i] + 32;
    } else if (str[i] >= 'a' && str[i] <= 'z'){
      str_id[i] = str[i];
    }
  }
  return str_id;
}

std::pair<std::string, std::string> splitFrontmatter(std::string in) noexcept
{
  std::string frontmatter{""};
  std::string body{""};

  std::istringstream stream(in);
  std::string l;
  bool firstLine{true};
  bool inFrontmatter{false};
  bool prependNewline{false};
  while (std::getline(stream, l)) {
    if (firstLine && l.substr(0, 3) == "---") {
      firstLine = false;
      inFrontmatter = true;
      prependNewline = false;
      continue;
    }
    if (inFrontmatter && l.substr(0, 3) == "---") {
      inFrontmatter = false;
      prependNewline = false;
      continue;
    }
    if (inFrontmatter) {
      if (prependNewline) {
        frontmatter += "\n";
      }
      frontmatter += l;
    } else {
      if (prependNewline) {
        body += "\n";
      }
      body += l;
    }
    prependNewline = true;
  }
  return {frontmatter, body};
}

std::map<std::string, std::string> extractData(std::string in) noexcept
{
  std::map<std::string, std::string> data;

  std::istringstream stream(in);
  std::string l;
  while (std::getline(stream, l)) {
    auto i = l.find(":");
    if (i != std::string::npos) {
      std::string key = trim(l.substr(0, i));
      std::string value = trim(l.substr(i+1));
      data[key] = value;
    }
  }
  return data;
}

std::string exec(std::string const &cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, int32_t(*)(FILE *)> pipe(popen(cmd.c_str(), "r"),
      pclose);
  if (!pipe) {
    std::cerr << "Could not start external process" << std::endl;
    return result;
  }
  while (::fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  return result;
}

void parseJsonRecursive(nlohmann::json const &json,
    kainjow::mustache::data &data, std::string const &key) noexcept
{
  kainjow::mustache::data list = kainjow::mustache::data::type::list;
  for (auto &j : json) {
    kainjow::mustache::data list_item;
    for (auto &e : j.items()) {
      if (e.value().is_array()) {
        parseJsonRecursive(e.value(), list_item, e.key());
      } else {
        std::string k = e.key();
        
        std::string v;
        if (e.value().is_number()) {
          v = std::to_string(e.value().get<int32_t>());
        } else {
          v = e.value();
        }
        list_item.set(k, v);
      }
    }
    list.push_back(list_item);
  }
  data.set(key, list);
}

void parseData(kainjow::mustache::data &mdata, std::string src_path,
    std::string out_path, std::string key, std::string value) noexcept
{
  if (!value.empty() && value[0] == '!') {
    std::string cmd = value.substr(1);
    value = exec("cd " + src_path + " && SITE_PATH=\"" + out_path + "\" "
        + cmd);
  }
  try {
    auto json = nlohmann::json::parse(value);
    if (json.type() == nlohmann::json::value_t::array) {
      parseJsonRecursive(json, mdata, key);
    } else {
      for (nlohmann::json::iterator it = json.begin(); it != json.end(); ++it) {
        std::string k = it.key();
        std::string v = it.value();
        parseJsonRecursive(v, mdata, key + "." + k);
      }
    }
  } catch (...) {
    mdata.set(key, value);
  }
}

std::string stripTags(std::string const src)
{
  std::string leftTagsGone = "";
  {
    auto ps = split(src, '<');
    bool first = true;
    for (auto &p : ps) {
      if (!first) {
        leftTagsGone += "&lt;";
      }
      first = false;
      leftTagsGone += p;
    }
  }
  std::string allTagsGone = "";
  auto ps = split(leftTagsGone, '>');
  bool first = true;
  for (auto &p : ps) {
    if (!first) {
      allTagsGone += "&gt;";
    }
    first = false;
    allTagsGone += p;
  }
  return allTagsGone;
}

std::string style(std::string const src, bool &is_em, bool &is_strong,
    bool &is_emstrong, bool &is_code)
{
  auto format = [&src](size_t const &i, std::string const &m,
      bool const &require_non_alnum, std::vector<std::string> tags, bool &state
      ) -> std::pair<size_t, std::string> {
    if (src.length() - i < m.length()) {
      return {0, ""};
    }
    for (size_t j{0}; j < m.size(); j++) {
      if (m[j] != src[i+j]) {
        return {0, ""}; 
      }
    }
    // There is a match.
    std::string ret = "";
    if (!state) {
      if ((!require_non_alnum || i == 0) ||
         (require_non_alnum && !::isalnum(src[i-1]))) {
        for (auto t : tags) {
          ret += "<" + t + ">";
        }
      }
    } else {
      if ((!require_non_alnum || i == src.length() - m.size()) ||
         (require_non_alnum && !::isalnum(src[i+m.size()]))) {
        // Backwards.
        for (auto it = tags.rbegin(); it != tags.rend(); it++) {
          ret += "</" + *it + ">";
        }
      }
    }
    if (!ret.empty()) {
      state = !state;
    }
    return {m.size(), ret};
  };

  auto replace = [&src](size_t const &i, char const &m, std::string const &v
      ) -> std::pair<size_t, std::string> {
    if (src[i] == m) {
      return {1, v};
    }
    return {0, ""};
  };

  auto img = [&src](size_t const &i) -> std::pair<size_t, std::string> {
    if (i < src.length() - 2 && src[i] == '!' && src[i+1] == '[') {
      std::string ret = "<img class=\"inline\" ";
      
      size_t alt0 = src.find("[", i) + 1;
      size_t alt1 = src.find("]", alt0) - 1;
      std::string alt = src.substr(alt0, alt1 - alt0 + 1);
      ret += "alt=\"" + alt + "\" ";
      
      size_t imgSrc0 = src.find("(", alt1) + 1;
      size_t end = src.find(")", imgSrc0);
      if (end == std::string::npos) {
        return {0, ""};
      }
      size_t imgSrc1;
      bool hasTitle = src.find(" ", imgSrc0) < end;
      if (hasTitle) {
        imgSrc1 = src.find(" ", imgSrc0) - 1;
        size_t title0 = src.find("\"", imgSrc1) + 1;
        size_t title1 = src.find("\"", title0) - 1;
        std::string title = src.substr(title0, title1 - title0 + 1);
        ret += "title=\"" + title + "\" ";
      } else {
         imgSrc1 = src.find(")", imgSrc0) - 1;
      }
      std::string imgSrc = src.substr(imgSrc0, imgSrc1 - imgSrc0 + 1);
      ret += "src=\"" + imgSrc + "\">";
      
      return {end - i + 1, ret};
    }
    return {0, ""};
  };

  auto link = [&src, &img](size_t const &i) -> std::pair<size_t, std::string> {
    if (i < src.length() - 1 && src[i] == '[') {
      std::string ret = "<a ";
      
      size_t inner0 = i + 1;
      size_t inner1;
      std::string inner;
      if (src[inner0] == '!') {
        auto r = img(inner0);
        inner1 = r.first;
        inner = r.second;
      } else {
        inner1 = src.find("]", inner0) - 1;
        inner = src.substr(inner0, inner1 - inner0 + 1);
      }
      
      size_t href0 = src.find("(", inner1) + 1;
      size_t end = src.find(")", href0);
      if (end == std::string::npos) {
        return {0, ""};
      }
      size_t href1;
      bool hasTitle = src.find(" ", href0) < end;
      if (hasTitle) {
        href1 = src.find(" ", href0) - 1;
        size_t title0 = src.find("\"", href1) + 1;
        size_t title1 = src.find("\"", title0) - 1;
        std::string title = src.substr(title0, title1 - title0 + 1);
        ret += "title=\"" + title + "\" ";
      } else {
         href1 = src.find(")", href0) - 1;
      }
      
      std::string href = src.substr(href0, href1 - href0 + 1);
      ret += "href=\"" + href + "\">" + inner + "</a>";
      
      return {end - i + 1, ret};
    }
    
    return {0, ""};
  };

  std::string dst{""};
  size_t i{0};

  auto tryAdd = [&dst, &i](std::pair<size_t, std::string> r) -> bool {
    if (r.first > 0) {
      dst += r.second;
      i += r.first;
      return true;
    }
    return false;
  };

  while (i < src.size()) {
    if (tryAdd(replace(i, '<', "&lt;"))) {
      continue;
    }
    if (tryAdd(replace(i, '>', "&gt;"))) {
      continue;
    }
    if (tryAdd(format(i, "`", false, {"code"}, is_code))) {
      continue;
    }
    if (!is_code) {
      if (tryAdd(img(i))) {
        continue;
      }
      if (tryAdd(link(i))) {
        continue;
      }
      if (tryAdd(format(i, "***", false, {"em", "strong"}, is_emstrong))) {
        continue;
      }
      if (tryAdd(format(i, "___", true, {"em", "strong"}, is_emstrong))) {
        continue;
      }
      if (tryAdd(format(i, "**", false, {"strong"}, is_strong))) {
        continue;
      }
      if (tryAdd(format(i, "__", true, {"strong"}, is_strong))) {
        continue;
      }
      if (tryAdd(format(i, "*", false, {"em"}, is_em))) {
        continue;
      }
      if (tryAdd(format(i, "_", true, {"em"}, is_em))) {
        continue;
      }
    }
    // Nothing special with this character. Just add it.
    dst += src[i];
    i++;
  }
  
  return dst;
}

std::string renderMarkdown(std::string src) noexcept
{
  enum class Block {
    ListAsterisk,
    ListPlus,
    ListMinus,
    ListEnum,
    Code,
    Blockquote,
    Table
  };

  std::string dst;

  std::vector<Block> blocks;

  bool in_paragraph = false;

  bool is_em = false;
  bool is_strong = false;
  bool is_emstrong = false;
  bool is_code = false;
      
  std::istringstream stream(src);
  std::string l;
  while (std::getline(stream, l)) {
    std::string lt = trim(l);

    // Code
    if (lt.substr(0, 3) == "```") {
      if (!blocks.empty() && blocks.back() == Block::Code) {
        dst += "</code></pre>\n\n";
        blocks.pop_back();
        continue;
      } else {
        std::string lang = lt.substr(3);
        std::string cls = "";
        if (!lang.empty()) {
          cls = " class='language-" + lang + "'";
        }
        dst += "<pre><code" + cls + ">";
        blocks.push_back(Block::Code);
        continue;
      }
    }

    if (!blocks.empty() && blocks.back() == Block::Code) {
      dst += stripTags(l) + "\n";
      continue;
    }

    // Header
    {
      bool rendered = false;
      for (uint32_t i{6}; i >= 1; i--) {
        std::string needle(i, '#');
        if (lt.substr(0, i) == needle) {
          std::string n = std::to_string(i);
          std::string title = trim(lt.substr(i));
          std::string title_styled = style(title, is_em, is_strong, is_emstrong,
              is_code);
          std::string title_id = idfy(title);
          dst += "<h" + n + "><a id='" + title_id + "' aria-hidden='true'>"
            + "</a>" + title_styled + "</h" + n + ">\n\n";
          rendered = true;
          break;
        }
      }
      if (rendered) {
        continue;
      }
    }

    // Unordered list
    // TODO: Support nested lists. Trim spaces.
    {
      bool rendered = false;
      bool list_new = true;
      bool list_type_change = false;
      bool list_item_asterisk = false;
      bool list_item_plus = false;
      bool list_item_minus = false;
      if (lt.size() >= 2 && lt[0] == '*' && lt[1] == ' ') {
        list_item_asterisk = true;
        if (!blocks.empty() && blocks.back() == Block::ListAsterisk) {
          list_new = false;
        } else {
          if (!blocks.empty() && (blocks.back() == Block::ListPlus
              || blocks.back() == Block::ListMinus)) {
            list_type_change = true;
            blocks.pop_back();
          }
          blocks.push_back(Block::ListAsterisk);
        }
      }
      if (lt.size() >= 2 && lt[0] == '+' && lt[1] == ' ') {
        list_item_plus = true;
        if (!blocks.empty() && blocks.back() == Block::ListPlus) {
          list_new = false;
        } else {
          if (!blocks.empty() && (blocks.back() == Block::ListAsterisk
              || blocks.back() == Block::ListMinus)) {
            list_type_change = true;
            blocks.pop_back();
          }
          blocks.push_back(Block::ListPlus);
        }
      }
      if (lt.size() >= 2 && lt[0] == '-' && lt[1] == ' ') {
        list_item_minus = true;
        if (!blocks.empty() && blocks.back() == Block::ListMinus) {
          list_new = false;
        } else {
          if (!blocks.empty() && (blocks.back() == Block::ListAsterisk
              || blocks.back() == Block::ListPlus)) {
            list_type_change = true;
            blocks.pop_back();
          }
          blocks.push_back(Block::ListMinus);
        }
      }
      if (list_item_asterisk || list_item_plus || list_item_minus) {
        size_t i = lt.find(" ");
        std::string text = trim(lt.substr(i));
        std::string text_styled = style(text, is_em, is_strong, is_emstrong,
            is_code);
        if (list_type_change) {
          dst += "</ul>\n\n";
        }
        if (list_new) {
          dst += "<ul>\n";
        }
        dst += "<li>" + text_styled + "</li>\n";
        rendered = true;
      }
      if (!list_item_asterisk && !blocks.empty()
          && blocks.back() == Block::ListAsterisk) {
        dst += "</ul>\n\n";
        blocks.pop_back();
      }
      if (!list_item_plus && !blocks.empty()
          && blocks.back() == Block::ListPlus) {
        dst += "</ul>\n\n";
        blocks.pop_back();
      }
      if (!list_item_minus && !blocks.empty()
          && blocks.back() == Block::ListMinus) {
        dst += "</ul>\n\n";
        blocks.pop_back();
      }
      if (rendered) {
        continue;
      }
    }

    // Enum list
    {
      bool rendered = false;
      bool list_new = true;
      bool list_item_enum = false;
      if (lt.size() > 0 && lt[0] >= '0' && lt[0] <= '9') {
        list_item_enum = true;
        if (!blocks.empty() && blocks.back() == Block::ListEnum) {
          list_new = false;
        } else {
          blocks.push_back(Block::ListEnum);
        }
      }
      if (list_item_enum) {
        size_t i = lt.find(" ");
        std::string text = trim(lt.substr(i));
        std::string text_styled = style(text, is_em, is_strong, is_emstrong,
            is_code);
        if (list_new) {
          dst += "<ol>\n";
        }
        dst += "<li>" + text_styled + "</li>";
        rendered = true;
      }
      if (!list_item_enum && !blocks.empty()
          && blocks.back() == Block::ListEnum) {
        dst += "</ol>\n\n";
        blocks.pop_back();
      }
      if (rendered) {
        continue;
      }
    }

    // Table
    // TODO: Simplification. No is_code from the previous line, and no code
    // on the line.
    if (!is_code && lt.find('`') == std::string::npos ) {
      auto cells = split(lt, '|');
      if (cells.size() > 1) {
        bool isHeader = false;
        if (blocks.empty() || blocks.back() != Block::Table) {
          dst += "<table>\n<thead>\n";
          blocks.push_back(Block::Table);
          isHeader = true;
        }
        dst += "<tr>\n";

        {
          bool isDashedLine = true;
          for (auto const &c : cells) {
            if (c.empty()) {
              continue;
            }
            if (c.find("---") == std::string::npos) {
              isDashedLine = false;
              break;
            }
          }
          if (isDashedLine) {
            continue;
          }
        }

        for (auto const &c : cells) {
          if (c.empty()) {
            continue;
          }
          std::string text_styled = style(c, is_em, is_strong, is_emstrong,
              is_code);
          if (isHeader) {
            dst += "<th>" + text_styled + "</th>\n";
          } else {
            dst += "<td>" + text_styled + "</td>\n";
          }
        }
        dst += "</tr>\n";
        if (isHeader) {
          dst += "</thead>\n<tbody>\n";
        }
        continue;
      }
      if (!blocks.empty() && blocks.back() == Block::Table) {
        dst += "</tbody>\n</table>\n\n";
        blocks.pop_back();
      }
    }
    
    // Block quote
    if (lt.substr(0, 1) == ">") {
      if (blocks.empty() || blocks.back() != Block::Blockquote) {
        dst += "<blockquote>\n";
        blocks.push_back(Block::Blockquote);
      }
      std::string text = trim(lt.substr(1));
      std::string text_styled = style(text, is_em, is_strong, is_emstrong,
          is_code);
      dst += text_styled + " ";
      continue;
    }
    if (!blocks.empty() && blocks.back() == Block::Blockquote) {
      dst += "</p>\n</blockquote>\n\n";
      in_paragraph = false;
      blocks.pop_back();
    }

    if (in_paragraph && lt.empty()) {
      dst += "</p>\n\n";
      in_paragraph = false;
      continue;
    }

    if (!lt.empty()) {
      if (!in_paragraph) { 
        dst += "<p>\n";
        in_paragraph = true;
      }
      dst += style(l, is_em, is_strong, is_emstrong, is_code) + "\n";
    }
  }
  return dst;
}

void transform(std::string src_rel_path, std::string out_rel_path) noexcept
{
  std::string src_path = std::filesystem::absolute(src_rel_path);
  std::string out_path = std::filesystem::absolute(out_rel_path);

  std::string root_path = src_path + "/root";
  //std::string post_path = src_path + "/post";
  std::string layout_path = src_path + "/layout";

  std::map<std::string,
    std::pair<std::string, std::map<std::string, std::string>>> layouts;
  for (auto const &entry : std::filesystem::directory_iterator(layout_path)) {
    auto p = entry.path();
    if (entry.is_directory()) {
      continue;
    }
    
    if (p.extension() == ".html") {
      std::ifstream ifs(p.string());
      std::string content((std::istreambuf_iterator<char>(ifs)),
          (std::istreambuf_iterator<char>()));
      auto r = splitFrontmatter(content);

      std::string frontmatter = r.first;
      std::string body = r.second;

      auto data = extractData(frontmatter);

      std::string name = p.filename();
      layouts[name] = {body, data};
    }
  }

  std::function<void(std::string, std::string)> parse_root;
  parse_root = [&parse_root, &src_path, &root_path, &out_path, &layouts](
      std::string parent, std::string dir) {
    for (auto const &entry : std::filesystem::directory_iterator(dir)) {
      auto p = entry.path();
      std::string rel_path = p.string().substr(root_path.length());
      if (entry.is_directory()) {
        std::filesystem::create_directories(out_path + rel_path);
        parse_root(parent + dir, p.string());
        continue;
      }
      
      if (p.extension() == ".md" || p.extension() == ".html"
          || p.extension() == ".css" || p.extension() == ".js") {
        std::ifstream ifs(p.string());
        std::string content((std::istreambuf_iterator<char>(ifs)),
            (std::istreambuf_iterator<char>()));
        auto r = splitFrontmatter(content);

        std::string frontmatter = r.first;
        std::string body = r.second;

        auto data = extractData(frontmatter);

        std::string page;
        {
          kainjow::mustache::data mdata;
          for (auto d : data) {
            parseData(mdata, src_path, out_path, d.first, d.second);
          }

          kainjow::mustache::mustache templ{body};
          page = templ.render(mdata);

          if (p.extension() == ".md") { 
            page = renderMarkdown(page);
          }
        }

        if (data.find("layout") != data.end()) {
          std::string name = data["layout"];
          std::string lbody = layouts[name].first;
          auto ldata = layouts[name].second;

          kainjow::mustache::data mdata;
          for (auto d : ldata) {
            parseData(mdata, src_path, out_path, d.first, d.second);
          }
          for (auto d : data) {
            parseData(mdata, src_path, out_path, d.first, d.second);
          }
          mdata.set("content", page);
          kainjow::mustache::mustache templ{lbody};
          page = templ.render(mdata);
        }

        if (p.extension() == ".md") {
          rel_path = rel_path.substr(0, rel_path.length() - 2) + "html";
        }

        std::ofstream d(out_path + rel_path);
        d << page;
        d.close();
      } else {
        std::ifstream s(p.string(), std::ios::binary);
        std::ofstream d(out_path + rel_path, std::ios::binary);
        d << s.rdbuf();
        s.close();
        d.close();
      }
    }
  };
  parse_root("", root_path);
}

}

#endif
