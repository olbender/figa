/*
 * Copyright (C) 2019 Ola Benderius
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TINYH2
#define TINYH2

#ifndef TINYH2_BUFFER_SIZE
#define TINYH2_BUFFER_SIZE 16393 // Max data size + 9
#endif

#include <bitset>

#include <array>
#include <atomic>
#include <cmath>
#include <cstring>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <iostream>
#include <iomanip>
#include <thread>
#include <vector>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

namespace tinyh2 {

std::atomic<bool> got_signal(false);

inline void gotSignal(int32_t) {
  got_signal.store(true);
}

inline void printHex(uint8_t const *data, uint32_t datalen) noexcept
{
  std::cout << std::hex;
  for (uint32_t i = 0; i < datalen; ++i) {
    uint32_t row = i / 16;
    uint32_t elem = i % 16;
    if (elem == 0) {
      std::cout << std::setfill('0') << std::setw(8)
        << (row * 16) << ":  ";
    }
    std::cout << std::setfill('0') << std::setw(2)
      << static_cast<uint32_t>(data[i]) << " ";
    if ((i+1) % 8 == 0) {
      std::cout << " ";
    }
    if (elem == 15 || i == datalen - 1) {
      if (i == datalen - 1) {
        uint32_t extra = 2 - (elem+1) / 8;
        for (uint32_t k = 0; k < 3 * (15 - elem) + extra; ++k) {
          std::cout << " ";
        }
      }
      for (int32_t j = elem; j >= 0; --j) {
        if (data[i-j] >= 32 && data[i-j] <= 126) {
          std::cout << data[i-j];
        } else {
          std::cout << ".";
        }
      }
      std::cout << std::endl;
    }
  }
  std::cout << std::dec;
}

inline std::vector<std::string> split(std::string const &str,
    char const &delimiter) noexcept {
  std::vector<std::string> parts;
  std::string::size_type prev{0};
  for (std::string::size_type i{str.find_first_of(delimiter, prev)};
       i != std::string::npos;
       prev = i + 1, i = str.find_first_of(delimiter, prev)) {
    if (i != prev) {
      parts.emplace_back(str.substr(prev, i - prev));
    } else {
      parts.emplace_back("");
    }
  }
  if (prev < str.size()) {
    parts.emplace_back(str.substr(prev, str.size() - prev));
  } else if (prev > 0) {
    parts.emplace_back("");
  }
  return parts;
}

inline std::string getUrlHash(std::string const &url) noexcept
{
  int32_t qpos = url.find('?');
  int32_t hpos = url.find('#');
  std::string hash;
  if (qpos != -1 && hpos != -1) {
    if (qpos < hpos) {
      hash = url.substr(hpos + 1);
    } else {
      hash = url.substr(hpos + 1);
    }
  } else if (hpos != -1) {
    hash = url.substr(hpos + 1);
  }
  return hash;
}

inline std::string getUrlPath(std::string const &url) noexcept
{
  int32_t qpos = url.find('?');
  int32_t hpos = url.find('#');
  std::string path;
  if (qpos == -1 && hpos == -1) {
    path = url;
  } else if (qpos != -1 && hpos != -1) {
    if (qpos < hpos) {
      path = url.substr(0, qpos);
    } else {
      path = url.substr(0, hpos);
    }
  } else if (qpos != -1) {
    path = url.substr(0, qpos);
  } else if (hpos != -1) {
    path = url.substr(0, hpos);
  }
  return path;
}

inline std::vector<std::string> getUrlPathParts(std::string const &url) noexcept
{
  std::vector<std::string> parts;
  std::string path = getUrlPath(url);

  uint32_t pos{1};
  while (true) {
    size_t npos = path.substr(pos).find("/");
    parts.push_back(path.substr(pos, npos));
    if (npos == std::string::npos) {
      break;
    }
    pos += npos + 1;
  }
  return parts;
}

inline std::map<std::string, std::vector<std::string>> getUrlQuery(
    std::string const &url) noexcept
{
  int32_t qpos = url.find('?');
  int32_t hpos = url.find('#');
  std::string query;
  std::map<std::string, std::vector<std::string>> queryFields;
  if (qpos != -1 && hpos != -1) {
    if (qpos < hpos) {
      query = url.substr(qpos + 1, hpos - qpos - 1);
    }
  } else if (qpos != -1) {
    query = url.substr(qpos + 1);
  }
  while (query.length() > 0) {
    int32_t epos = query.find('=');
    int32_t apos = query.find('&');
    std::string name;
    std::string value;
    if (epos == -1 && apos == -1) {
      name = query;
    } else if (epos != -1 && apos != -1) {
      if (epos < apos) {
        name = query.substr(0, epos);
        value = query.substr(epos + 1, apos - epos - 1);
      } else {
        name = query.substr(0, apos);
      }
    } else if (epos != -1) {
      name = query.substr(0, epos);
      value = query.substr(epos + 1);
    } else if (apos != -1) {
      name = query.substr(0, apos);
    }
    if (apos != -1) {
      query = query.substr(apos + 1);
    } else {
      query = "";
    }
    if (!queryFields.count(name)) {
      queryFields[name] = {};
    }
    queryFields[name].push_back(value);
  }
  return queryFields;
}

inline std::string decodeBase64(std::string const &input,
    std::string const &alphabet) noexcept
{
  uint32_t len = input.size();
  if (len % 4 != 0) {
    len += 4 - (len % 4);
  }
  uint8_t counter{0};
  std::array<char, 4> buffer;
  std::string decoded;
  for (uint32_t i{0}; i < len; i++) {
    char ci;
    if (i < input.size()) {
      ci = input.at(i);
    } else {
      ci = '=';
    }
    char c;
    for (c = 0; c < 64 && (alphabet.at(static_cast<uint8_t>(c)) != ci); c++) {
    }
    buffer[counter++] = c;
    if (4 == counter) {
      decoded.push_back(static_cast<char>((buffer[0] << 2) + (buffer[1] >> 4)));
      if (64 != buffer[2]) {
        decoded.push_back(
            static_cast<char>((buffer[1] << 4) + (buffer[2] >> 2)));
      }
      if (64 != buffer[3]) {
        decoded.push_back(static_cast<char>((buffer[2] << 6) + buffer[3]));
      }
      counter = 0;
    }
  }
  return decoded;
}

inline std::string decodeBase64(std::string const &input) noexcept
{
  std::string const alphabet{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"};
  return decodeBase64(input, alphabet);
}

inline std::string decodeBase64Url(std::string const &input) noexcept
{
  std::string const alphabet{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"};
  return decodeBase64(input, alphabet);
}

inline std::string encodeBase64(std::string const &input,
    std::string const &alphabet) noexcept
{
  std::string r;
  auto length{input.length()};
  uint32_t i{0};
  uint32_t v{0};

  while (length > 2) {
    v = static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++))) << 16;
    v |= static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++))) << 8;
    v |= static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++)));
    r += alphabet.at((v & 0xFC0000) >> 18);
    r += alphabet.at((v & 0x3F000) >> 12);
    r += alphabet.at((v & 0xFC0) >> 6);
    r += alphabet.at(v & 0x3F);
    length -= 3;
  }
  if (length == 2) {
    v = static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++))) << 16;
    v |= static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++))) << 8;
    r += alphabet.at((v & 0xFC0000) >> 18);
    r += alphabet.at((v & 0x3F000) >> 12);
    r += alphabet.at((v & 0xFC0) >> 6);
  } else if (length == 1) {
    v = static_cast<uint32_t>(static_cast<unsigned char>(input.at(i++))) << 16;
    r += alphabet.at((v & 0xFC0000) >> 18);
    r += alphabet.at((v & 0x3F000) >> 12);
  }
  return r;
}

inline std::string encodeBase64(std::string const &input) noexcept
{
  std::string const alphabet{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"};
  return encodeBase64(input, alphabet);
}

inline std::string encodeBase64Url(std::string const &input) noexcept
{
  std::string const alphabet{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"};
  return encodeBase64(input, alphabet);
}

inline std::string sha256(std::string &input, std::array<uint8_t, 64> salt)
  noexcept
{
  EVP_MD const *md = EVP_sha256();
  EVP_MD_CTX *ctx = EVP_MD_CTX_create();
  EVP_DigestInit_ex(ctx, md, nullptr);

  uint32_t hash_size = EVP_MD_size(md);

  std::string input_hash_with_salt(hash_size + salt.size(), ' ');
  ::memcpy(reinterpret_cast<void *>(input_hash_with_salt.data() + hash_size),
      salt.data(), salt.size());
  EVP_DigestUpdate(ctx, input.data(), input.size());
  EVP_DigestFinal_ex(ctx,
      reinterpret_cast<unsigned char *>(input_hash_with_salt.data()),
      &hash_size);

  std::string signature(hash_size, ' ');
  EVP_DigestUpdate(ctx,
      reinterpret_cast<unsigned char *>(input_hash_with_salt.data()),
      input_hash_with_salt.size());
  EVP_DigestFinal_ex(ctx, reinterpret_cast<unsigned char *>(signature.data()),
      &hash_size);

  EVP_MD_CTX_destroy(ctx);

  return signature;
}

inline uint64_t getTimestamp() noexcept
{
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
      std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}

inline bool isIpInRange(std::string const &ip, std::string const &ranges)
  noexcept
{
  int32_t ipn{0};
  {
    auto ipp = split(ip, '.');
    if (ipp.size() == 4) {
      try {
        ipn = (std::stoi(ipp[0]) << 24) +
          (std::stoi(ipp[1]) << 16) +
          (std::stoi(ipp[2]) << 8) +
          std::stoi(ipp[3]);
      } catch (...) {
        return false;
      }
    } else {
      return false;
    }
  }
  for (auto &r : split(ranges, ',')) {
    auto rpair = split(r, '/');
    if (rpair.size() == 2) {
      std::string netmask = rpair[1];

      int32_t netmaski{0};
      try {
        netmaski = std::stoi(netmask);
        if (netmaski < 0 || netmaski > 32) {
          continue;
        }
      } catch (...) {
        continue;
      }

      uint32_t netmaskn = static_cast<uint32_t>(std::pow(2, netmaski) - 1)
        << (32 - netmaski);

      std::string netip = rpair[0];
      uint32_t netipn{0};
      auto netipp = split(netip, '.');
      if (netipp.size() == 4) {
        try {
          netipn = (std::stoi(netipp[0]) << 24) +
            (std::stoi(netipp[1]) << 16) +
            (std::stoi(netipp[2]) << 8) +
            std::stoi(netipp[3]);
        } catch (...) {
          continue;
        }

        if ((ipn & netmaskn) == (netipn & netmaskn)) {
          return true;
        }
      }
    }    
  }
  return false;
}

enum Verbosity { NONE = 0, ON_ERROR, INFO, DEBUG };

class Http2Encoder {
  public:
    Http2Encoder(uint8_t *, uint32_t) noexcept;
    Http2Encoder(Http2Encoder const &) = delete;
    Http2Encoder(Http2Encoder &&) = delete;
    ~Http2Encoder() noexcept;

    Http2Encoder &operator=(Http2Encoder const &) = delete;
    Http2Encoder &operator=(Http2Encoder &&) = delete;

    int32_t encodeData(void const *, uint32_t, uint32_t, bool) noexcept;
    int32_t encodeHeader(std::string, uint32_t, uint32_t, uint32_t, bool)
      noexcept;
    int32_t encodePing(uint64_t, bool) noexcept;
    int32_t encodePreface() noexcept;
    int32_t encodeSettingsAck() noexcept;
    int32_t encodeSettingsDefaultClient() noexcept;
    int32_t encodeSettingsDefaultServer() noexcept;
    int32_t encodeWindowUpdate(uint32_t, uint32_t) noexcept;
    uint8_t *data() const noexcept;
    uint32_t length() const noexcept;
    void reset() noexcept;
    int32_t setPayloadLengthMax(uint32_t) noexcept;
    uint32_t space() const noexcept;

  private:
    int32_t encodeFrame(uint32_t, uint8_t, uint8_t, uint32_t) noexcept;

    uint32_t m_payload_length_max;
    uint8_t *m_buf_start;
    uint8_t *m_buf_pos;
    uint8_t *m_buf_end;
};

inline Http2Encoder::Http2Encoder(uint8_t *buf_start, uint32_t buf_len)
  noexcept:
  m_payload_length_max{16384},
  m_buf_start{buf_start},
  m_buf_pos{buf_start},
  m_buf_end{buf_start + buf_len}
{
}

inline Http2Encoder::~Http2Encoder()
{
}

inline int32_t Http2Encoder::encodeData(void const *content,
    uint32_t content_length, uint32_t stream_id, bool is_stream_end) noexcept
{
  uint8_t flags = is_stream_end ? 1 : 0;
  if (!encodeFrame(content_length, 0, flags, stream_id)) {
    return -1;
  }

  ::memcpy(m_buf_pos, content, content_length);
  m_buf_pos += content_length;
  return 1;
}

inline int32_t Http2Encoder::encodeFrame(uint32_t payload_length, uint8_t type,
    uint8_t flags, uint32_t stream_id) noexcept
{
  if (payload_length > m_payload_length_max) {
    return -1;
  }
  if (payload_length > 16777216) {
    return -2;
  }
  if (stream_id > 2147483648) {
    return -3;
  }

  uint32_t payload_length_n{htonl(payload_length) >> 8};
  uint32_t stream_id_n{htonl(stream_id)};

  ::memcpy(m_buf_pos, &payload_length_n, 3);
  ::memcpy(m_buf_pos + 3, &type, 1);
  ::memcpy(m_buf_pos + 4, &flags, 1);
  ::memcpy(m_buf_pos + 5, &stream_id_n, 4);
  m_buf_pos += 9;
  return 1;
}

inline int32_t Http2Encoder::encodeHeader(std::string content_type,
    uint32_t status_code, uint32_t content_length, uint32_t stream_id,
    bool is_stream_end) noexcept
{
  static auto const packHeaderInteger{
    [](uint32_t n, uint32_t i) -> std::string {
      uint32_t k = static_cast<uint32_t>(std::pow(2, n) - 1);

      std::string ret;
      if (i < k) {
        ret = static_cast<char>(i);
        return ret;
      }
      ret = static_cast<char>(k);
      uint32_t j = i - k;
      while (j >= 128) {
        ret += static_cast<char>(j % 128 + 128);
        j = j / 128;
      }
      ret += static_cast<char>(j);
      return ret;
    }};

  static auto const packHeaderLiteral{
    [](std::string str, bool use_huffman) -> std::string {

      static std::vector<std::pair<uint32_t, uint32_t>> huffman_table{
        {13, 0x1ff8}, {23, 0x7fffd8}, {28, 0xfffffe2}, {28, 0xfffffe3},
          {28, 0xfffffe4}, {28, 0xfffffe5}, {28, 0xfffffe6}, {28, 0xfffffe7},
          {28, 0xfffffe8}, {24, 0xffffea}, {30, 0x3ffffffc}, {28, 0xfffffe9},
          {28, 0xfffffea}, {30, 0x3ffffffd}, {28, 0xfffffeb}, {28, 0xfffffec},
          {28, 0xfffffed}, {28, 0xfffffee}, {28, 0xfffffef}, {28, 0xffffff0},
          {28, 0xffffff1}, {28, 0xffffff2}, {30, 0x3ffffffe}, {28, 0xffffff3},
          {28, 0xffffff4}, {28, 0xffffff5}, {28, 0xffffff6}, {28, 0xffffff7},
          {28, 0xffffff8}, {28, 0xffffff9}, {28, 0xffffffa}, {28, 0xffffffb},
          {6, 0x14}, {10, 0x3f8}, {10, 0x3f9}, {12, 0xffa}, {13, 0x1ff9},
          {6, 0x15}, {8, 0xf8}, {11, 0x7fa}, {10, 0x3fa}, {10, 0x3fb},
          {8, 0xf9}, {11, 0x7fb}, {8, 0xfa}, {6, 0x16}, {6, 0x17}, {6, 0x18},
          {5, 0x0}, {5, 0x1}, {5, 0x2}, {6, 0x19}, {6, 0x1a}, {6, 0x1b},
          {6, 0x1c}, {6, 0x1d}, {6, 0x1e}, {6, 0x1f}, {7, 0x5c}, {8, 0xfb},
          {15, 0x7ffc}, {6, 0x20}, {12, 0xffb}, {10, 0x3fc}, {13, 0x1ffa},
          {6, 0x21}, {7, 0x5d}, {7, 0x5e}, {7, 0x5f}, {7, 0x60}, {7, 0x61},
          {7, 0x62}, {7, 0x63}, {7, 0x64}, {7, 0x65}, {7, 0x66}, {7, 0x67},
          {7, 0x68}, {7, 0x69}, {7, 0x6a}, {7, 0x6b}, {7, 0x6c}, {7, 0x6d},
          {7, 0x6e}, {7, 0x6f}, {7, 0x70}, {7, 0x71}, {7, 0x72}, {8, 0xfc},
          {7, 0x73}, {8, 0xfd}, {13, 0x1ffb}, {19, 0x7fff0}, {13, 0x1ffc},
          {14, 0x3ffc}, {6, 0x22}, {15, 0x7ffd}, {5, 0x3}, {6, 0x23}, {5, 0x4},
          {6, 0x24}, {5, 0x5}, {6, 0x25}, {6, 0x26}, {6, 0x27}, {5, 0x6},
          {7, 0x74}, {7, 0x75}, {6, 0x28}, {6, 0x29}, {6, 0x2a}, {5, 0x7},
          {6, 0x2b}, {7, 0x76}, {6, 0x2c}, {5, 0x8}, {5, 0x9}, {6, 0x2d},
          {7, 0x77}, {7, 0x78}, {7, 0x79}, {7, 0x7a}, {7, 0x7b}, {15, 0x7ffe},
          {11, 0x7fc}, {14, 0x3ffd}, {13, 0x1ffd}, {28, 0xffffffc},
          {20, 0xfffe6}, {22, 0x3fffd2}, {20, 0xfffe7}, {20, 0xfffe8},
          {22, 0x3fffd3}, {22, 0x3fffd4}, {22, 0x3fffd5}, {23, 0x7fffd9},
          {22, 0x3fffd6}, {23, 0x7fffda}, {23, 0x7fffdb}, {23, 0x7fffdc},
          {23, 0x7fffdd}, {23, 0x7fffde}, {24, 0xffffeb}, {23, 0x7fffdf},
          {24, 0xffffec}, {24, 0xffffed}, {22, 0x3fffd7}, {23, 0x7fffe0},
          {24, 0xffffee}, {23, 0x7fffe1}, {23, 0x7fffe2}, {23, 0x7fffe3},
          {23, 0x7fffe4}, {21, 0x1fffdc}, {22, 0x3fffd8}, {23, 0x7fffe5},
          {22, 0x3fffd9}, {23, 0x7fffe6}, {23, 0x7fffe7}, {24, 0xffffef},
          {22, 0x3fffda}, {21, 0x1fffdd}, {20, 0xfffe9}, {22, 0x3fffdb},
          {22, 0x3fffdc}, {23, 0x7fffe8}, {23, 0x7fffe9}, {21, 0x1fffde},
          {23, 0x7fffea}, {22, 0x3fffdd}, {22, 0x3fffde}, {24, 0xfffff0},
          {21, 0x1fffdf}, {22, 0x3fffdf}, {23, 0x7fffeb}, {23, 0x7fffec},
          {21, 0x1fffe0}, {21, 0x1fffe1}, {22, 0x3fffe0}, {21, 0x1fffe2},
          {23, 0x7fffed}, {22, 0x3fffe1}, {23, 0x7fffee}, {23, 0x7fffef},
          {20, 0xfffea}, {22, 0x3fffe2}, {22, 0x3fffe3}, {22, 0x3fffe4},
          {23, 0x7ffff0}, {22, 0x3fffe5}, {22, 0x3fffe6}, {23, 0x7ffff1},
          {26, 0x3ffffe0}, {26, 0x3ffffe1}, {20, 0xfffeb}, {19, 0x7fff1},
          {22, 0x3fffe7}, {23, 0x7ffff2}, {22, 0x3fffe8}, {25, 0x1ffffec},
          {26, 0x3ffffe2}, {26, 0x3ffffe3}, {26, 0x3ffffe4}, {27, 0x7ffffde},
          {27, 0x7ffffdf}, {26, 0x3ffffe5}, {24, 0xfffff1}, {25, 0x1ffffed},
          {19, 0x7fff2}, {21, 0x1fffe3}, {26, 0x3ffffe6}, {27, 0x7ffffe0},
          {27, 0x7ffffe1}, {26, 0x3ffffe7}, {27, 0x7ffffe2}, {24, 0xfffff2},
          {21, 0x1fffe4}, {21, 0x1fffe5}, {26, 0x3ffffe8}, {26, 0x3ffffe9},
          {28, 0xffffffd}, {27, 0x7ffffe3}, {27, 0x7ffffe4}, {27, 0x7ffffe5},
          {20, 0xfffec}, {24, 0xfffff3}, {20, 0xfffed}, {21, 0x1fffe6},
          {22, 0x3fffe9}, {21, 0x1fffe7}, {21, 0x1fffe8}, {23, 0x7ffff3},
          {22, 0x3fffea}, {22, 0x3fffeb}, {25, 0x1ffffee}, {25, 0x1ffffef},
          {24, 0xfffff4}, {24, 0xfffff5}, {26, 0x3ffffea}, {23, 0x7ffff4},
          {26, 0x3ffffeb}, {27, 0x7ffffe6}, {26, 0x3ffffec}, {26, 0x3ffffed},
          {27, 0x7ffffe7}, {27, 0x7ffffe8}, {27, 0x7ffffe9}, {27, 0x7ffffea},
          {27, 0x7ffffeb}, {28, 0xffffffe}, {27, 0x7ffffec}, {27, 0x7ffffed},
          {27, 0x7ffffee}, {27, 0x7ffffef}, {27, 0x7fffff0}, {26, 0x3ffffee},
          {30, 0x3fffffff}
      };

      static uint32_t const char_buf_size{32};
      static uint32_t const staging_buf_size{32};

      std::string ret;

      if (use_huffman) {
        std::vector<uint32_t> huffman;
        uint32_t huffman_len{0};

        uint32_t staging{0};
        uint32_t staging_pos{0};

        for (uint32_t i{0}; i < str.length(); ++i) {
          uint32_t const char_bit_count = huffman_table[str[i]].first;
          uint32_t const char_bits = huffman_table[str[i]].second;
          uint32_t char_pos{0};

          while (char_pos < char_bit_count) {
            uint32_t bits_available = char_bit_count - char_pos;
            uint32_t staging_space_available = staging_buf_size - staging_pos;
            uint32_t bits_to_copy =
              std::min(bits_available, staging_space_available);

            staging = (staging << bits_to_copy)
              | (char_bits << (char_buf_size - char_bit_count + char_pos))
              >> (char_buf_size - bits_to_copy);

            char_pos += bits_to_copy;
            staging_pos += bits_to_copy;

            if (staging_pos >= staging_buf_size) {
              uint32_t staging_n = htonl(staging);
              huffman.push_back(staging_n);
              huffman_len += sizeof(staging);
              staging = 0;
              staging_pos = 0;
            }
          }
        }

        if (staging_pos != 0) {
          uint32_t staging_space_available = staging_buf_size - staging_pos;
          staging = (staging << staging_space_available)
            | (std::numeric_limits<uint32_t>::max() >> staging_pos);
          uint32_t staging_n = htonl(staging);
          huffman.push_back(staging_n);
          huffman_len += (staging_pos + 7) / 8;
        }

        uint8_t *huffman_data = reinterpret_cast<uint8_t *>(huffman.data());
        ret = packHeaderInteger(7, huffman_len)
          + std::string(huffman_data, huffman_data + huffman_len);

        ret[0] = ret[0] | 0x80;
      } else {
        ret = packHeaderInteger(7, str.length()) + str;
      }

      return ret;
    }};

  static auto const packIndexedNameAndValue{
    [](uint32_t index) -> std::string {
      std::string ret = packHeaderInteger(7, index);
      ret[0] = ret[0] | 0x80;
      return ret;
    }};

  static auto const packIndexedNameIncrementalIndexedValue{
    [](uint32_t index, std::string value, bool use_huffman) -> std::string {
      std::string ret = packHeaderInteger(6, index);
      ret[0] = ret[0] | 0x40;
      ret += packHeaderLiteral(value, use_huffman);
      return ret;
    }};

  static auto const packIndexedNameNoIndexedValue{
    [](uint32_t index, std::string value, bool use_huffman) -> std::string {
      std::string ret = packHeaderInteger(4, index);
      ret += packHeaderLiteral(value, use_huffman);
      return ret;
    }};

  static auto const generateHttpDateTime{
    []() -> std::string {
      static std::mutex gmtime_mutex;
      std::stringstream ss;
      std::time_t t = std::time(nullptr);
      {
        std::lock_guard<std::mutex> const lock(gmtime_mutex);
        ss << std::put_time(std::gmtime(&t),"%a, %d %b %Y %T %Z");
      }
      return ss.str();
    }};

  std::string payload;
  switch (status_code) {
    case 200:
      payload += packIndexedNameAndValue(8);
      break;
    case 204:
      payload += packIndexedNameAndValue(9);
      break;
    case 206:
      payload += packIndexedNameAndValue(10);
      break;
    case 304:
      payload += packIndexedNameAndValue(11);
      break;
    case 400:
      payload += packIndexedNameAndValue(12);
      break;
    case 404:
      payload += packIndexedNameAndValue(13);
      break;
    case 500:
    default:
      // TODO: Is this complete?
      payload += packIndexedNameAndValue(14);
      break;
  }
  payload += packIndexedNameIncrementalIndexedValue(54,
      "Ola Benderius, tinyh2 v1", true);
  payload += packIndexedNameIncrementalIndexedValue(33, generateHttpDateTime(),
      true);
  payload += packIndexedNameIncrementalIndexedValue(31, content_type, true);

  if (content_type != "text/event-stream") {
    payload += packIndexedNameNoIndexedValue(28, std::to_string(content_length),
        false);
  }

  uint8_t flags = 4;
  if (is_stream_end) {
    flags += 1;
  }
  if (!encodeFrame(payload.length(), 1, 4, stream_id)) {
    return -1;
  }

  ::memcpy(m_buf_pos, payload.data(), payload.length());
  m_buf_pos += payload.length();
  return 1;
}

inline uint8_t *Http2Encoder::data() const noexcept
{
  return m_buf_start;
}

inline int32_t Http2Encoder::encodePing(uint64_t opaque_data, bool ack) noexcept
{
  uint8_t flags = static_cast<uint8_t>(ack);
  if (!encodeFrame(8, 6, flags, 0)) {
    return -1;
  }

  uint64_t opaque_data_n{htobe64(opaque_data)};

  ::memcpy(m_buf_pos, &opaque_data_n, 8);
  m_buf_pos += 8;
  return 1;
}

inline int32_t Http2Encoder::encodePreface() noexcept
{
  static std::vector<uint8_t> preface = {
    0x50, 0x52, 0x49, 0x20, 0x2a, 0x20, 0x48, 0x54, 0x54, 0x50, 0x2f, 0x32,
    0x2e, 0x30, 0x0d, 0x0a, 0x0d, 0x0a, 0x53, 0x4d, 0x0d, 0x0a, 0x0d, 0x0a
  };
  ::memcpy(m_buf_pos, preface.data(), 24);
  m_buf_pos += 24;
  return 1;
}

inline int32_t Http2Encoder::encodeSettingsAck() noexcept
{
  return encodeFrame(0, 4, 1, 0);
}

inline int32_t Http2Encoder::encodeSettingsDefaultClient() noexcept
{
  if (!encodeFrame(18, 4, 0, 0)) {
    return -1;
  }
  {
    uint16_t id_n{htons(2)};
    uint32_t enable_push_n{htonl(0)};
    ::memcpy(m_buf_pos, &id_n, 2);
    ::memcpy(m_buf_pos + 2, &enable_push_n, 4);
    m_buf_pos += 6;
  }
  {
    uint16_t id_n{htons(3)};
    uint32_t max_concurrent_streams_n{htonl(100)};
    ::memcpy(m_buf_pos, &id_n, 2);
    ::memcpy(m_buf_pos + 2, &max_concurrent_streams_n, 4);
    m_buf_pos += 6;
  }
  {
    uint16_t id_n{htons(4)};
    uint32_t initial_window_size_n{htonl(33554432)};
    ::memcpy(m_buf_pos, &id_n, 2);
    ::memcpy(m_buf_pos + 2, &initial_window_size_n, 4);
    m_buf_pos += 6;
  }
  return 1;
}

inline int32_t Http2Encoder::encodeSettingsDefaultServer() noexcept
{
  if (!encodeFrame(6, 4, 0, 0)) {
    return -1;
  }
  uint16_t id_n{htons(3)};
  uint32_t max_concurrent_streams_n{htonl(100)};
  ::memcpy(m_buf_pos, &id_n, 2);
  ::memcpy(m_buf_pos + 2, &max_concurrent_streams_n, 4);
  m_buf_pos += 6;
  return 1;
}

inline int32_t Http2Encoder::encodeWindowUpdate(
    uint32_t window_size_increment, uint32_t stream_id) noexcept
{
  if (!encodeFrame(4, 8, 0, stream_id)) {
    return -1;
  }

  uint32_t window_size_increment_n{htonl(window_size_increment)};

  ::memcpy(m_buf_pos, &window_size_increment_n, 4);

  m_buf_pos += 4;
  return 1;
}

inline uint32_t Http2Encoder::length() const noexcept
{
  return m_buf_pos - m_buf_start;
}

inline void Http2Encoder::reset() noexcept
{
  m_buf_pos = m_buf_start;
}

inline int32_t Http2Encoder::setPayloadLengthMax(uint32_t payload_length_max)
  noexcept
{
  if (payload_length_max < 16384) {
    return -1;
  }
  if (payload_length_max > 16777215) {
    return -2;
  }
  m_payload_length_max = payload_length_max;
  return 1;
}

inline uint32_t Http2Encoder::space() const noexcept
{
  return m_buf_end - m_buf_pos;
}

class Http2Decoder {
  public:
    enum class StatusCode { NoError, UnknownType, DecodeError, FrameError };

    Http2Decoder(uint8_t *, uint32_t) noexcept;
    Http2Decoder(Http2Decoder const &) = delete;
    Http2Decoder(Http2Decoder &&) = delete;
    ~Http2Decoder() noexcept;

    Http2Decoder &operator=(Http2Decoder const &) = delete;
    Http2Decoder &operator=(Http2Decoder &&) = delete;

    std::pair<StatusCode, int32_t> decode(
        std::vector<std::pair<std::string, std::string>> &, uint32_t, uint32_t)
      noexcept;
    void onData(std::function<void(uint32_t, bool, std::string)>) noexcept;
    void onGoAway(std::function<void(uint32_t, uint32_t, std::string)>)
      noexcept;
    void onHeader(std::function<void(uint32_t, bool, bool,
          std::map<std::string, std::string>)>) noexcept;
    void onSettingsAck(std::function<void()>) noexcept;
    void onSettings(std::function<void(std::map<uint8_t, uint32_t>)>) noexcept;
    void onPing(std::function<void(uint64_t, bool)>) noexcept;
    void onPriority(std::function<void(uint32_t, bool, uint32_t,
          uint8_t)>) noexcept;
    void onPreface(std::function<void()>) noexcept;
    void onRstStream(std::function<void(uint32_t, uint32_t)>) noexcept;

    void onWindowUpdate(std::function<void(uint32_t, uint32_t)>) noexcept;

    uint8_t *data() const noexcept;
    uint32_t frameLength(uint32_t) const noexcept;
    uint32_t maxLength() const noexcept;
    void reset() noexcept;

  private:
    int32_t decodeData() noexcept;
    int32_t decodeGoAway() noexcept;
    int32_t decodeHeader(std::vector<std::pair<std::string, std::string>> &,
        uint32_t) noexcept;
    int32_t decodePing() noexcept;
    int32_t decodePreface() noexcept;
    int32_t decodePriority() noexcept;
    int32_t decodeRstStream() noexcept;
    int32_t decodeSettings() noexcept;
    int32_t decodeWindowUpdate() noexcept;
    uint8_t extract8(uint8_t *) const noexcept;
    uint32_t extract24(uint8_t *) const noexcept;
    std::pair<uint32_t, bool> extract31WithFirstBitFlag(uint8_t *) const
      noexcept;
    uint32_t extract32(uint8_t *) const noexcept;
    uint64_t extract64(uint8_t *) const noexcept;
    std::string extractString(uint8_t *, uint32_t) const noexcept;

    std::function<void(uint32_t, bool, std::string)> m_data_delegate;
    std::function<void(uint32_t, uint32_t, std::string)> m_go_away_delegate;
    std::function<void(uint32_t, bool, bool,
        std::map<std::string, std::string>)> m_header_delegate;
    std::function<void(uint64_t, bool)> m_ping_delegate;
    std::function<void(uint32_t, bool, uint32_t, uint8_t)> m_priority_delegate;
    std::function<void()> m_preface_delegate;
    std::function<void(uint32_t, uint32_t)> m_rst_stream_delegate;
    std::function<void()> m_settings_ack_delegate;
    std::function<void(std::map<uint8_t, uint32_t>)> m_settings_delegate;
    std::function<void(uint32_t, uint32_t)> m_window_update_delegate;

    uint8_t *m_buf_start;
    uint8_t *m_buf_pos;
    uint8_t *m_buf_end;
};

inline Http2Decoder::Http2Decoder(uint8_t *buf_start, uint32_t buf_len)
  noexcept:
  m_data_delegate{},
  m_go_away_delegate{},
  m_header_delegate{},
  m_ping_delegate{},
  m_priority_delegate{},
  m_preface_delegate{},
  m_rst_stream_delegate{},
  m_settings_ack_delegate{},
  m_settings_delegate{},
  m_window_update_delegate{},
  m_buf_start{buf_start},
  m_buf_pos{buf_start},
  m_buf_end{buf_start + buf_len}
{
}

inline Http2Decoder::~Http2Decoder()
{
}

inline uint8_t *Http2Decoder::data() const noexcept
{
  return m_buf_start;
}

inline std::pair<Http2Decoder::StatusCode, int32_t> Http2Decoder::decode(
    std::vector<std::pair<std::string, std::string>> &dynamic_table,
    uint32_t max_header_list_size, uint32_t length) noexcept
{
  uint8_t *start = m_buf_pos;
  while (m_buf_pos - start < length) {
    if (length - (m_buf_pos - start) < 9) {
      return {StatusCode::FrameError, 0};
    }
    uint8_t type = extract8(m_buf_pos + 3);
    int32_t r;
    switch (type) {
      case 0:
        r = decodeData();
        break;
      case 1:
        r = decodeHeader(dynamic_table, max_header_list_size);
        break;
      case 2:
        r = decodePriority();
        break;
      case 3:
        r = decodeRstStream();
        break;
      case 4:
        r = decodeSettings();
        break;
      case 6:
        r = decodePing();
        break;
      case 7:
        r = decodeGoAway();
        break;
      case 8:
        r = decodeWindowUpdate();
        break;
      case 32:
        r = decodePreface();
        break;
      default:
        return {StatusCode::UnknownType, type};
    };
    if (r < 0) {
      return {StatusCode::DecodeError, r};
    }
  }
  return {StatusCode::NoError, 0};
}

inline int32_t Http2Decoder::decodeData() noexcept
{
  uint32_t length = extract24(m_buf_pos);

  uint8_t flags = extract8(m_buf_pos + 4);
  bool is_end_stream = static_cast<bool>(flags & 0x1);
  bool is_padded = static_cast<bool>(flags & 0x8);

  uint32_t stream_id = extract32(m_buf_pos + 5);

  uint8_t pad_length{0};

  uint32_t data_length{length};

  uint32_t offset{0};
  if (is_padded) {
    pad_length = extract8(m_buf_pos + 9);
    offset++;
    data_length -= (1 + pad_length);
  }

  // TODO: In the end, check that we have the right number of padding bytes,
  // all zero. Or give PROTOCOL_ERROR

  if (m_data_delegate != nullptr) {
    std::string data(reinterpret_cast<char *>(m_buf_pos + 9 + offset),
        data_length);
    m_data_delegate(stream_id, is_end_stream, data);
  }

  m_buf_pos += 9 + length;
  return 1;
}

inline int32_t Http2Decoder::decodeGoAway() noexcept
{
  uint32_t length = extract24(m_buf_pos);
  if (m_go_away_delegate != nullptr) {
    std::pair<uint32_t, bool> last_stream_id_and_reserved_bit
      = extract31WithFirstBitFlag(m_buf_pos + 9);
    uint32_t const error_code = extract32(m_buf_pos + 13);

    uint32_t const debug_length = length - 8;
    std::string const debug = extractString(m_buf_pos + 17, debug_length);

    m_go_away_delegate(last_stream_id_and_reserved_bit.first, error_code,
        debug);
  }
  m_buf_pos += 9 + length;
  return 1;
}

inline int32_t Http2Decoder::decodeHeader(
    std::vector<std::pair<std::string, std::string>> &dynamic_table,
    uint32_t max_header_list_size) noexcept
{
  static auto const header_table{
    [](std::vector<std::pair<std::string, std::string>> const &dt,
        uint8_t v) -> std::pair<std::string, std::string> {
      if (v < 1 || v > (61 + dt.size())) {
        return std::pair<std::string, std::string>{"", ""};
      }
      static std::vector<std::pair<std::string, std::string>> static_table{
        {":authority", ""}, {":method", "GET"}, {":method", "POST"},
        {":path", "/"}, {":path", "/index.html"}, {":scheme", "http"},
        {":scheme", "https"}, {":status", "200"}, {":status", "204"},
        {":status", "206"}, {":status", "304"}, {":status", "400"},
        {":status", "404"}, {":status", "500"}, {"accept-charset", ""},
        {"accept-encoding", "gzip, deflate"}, {"accept-language", ""},
        {"accept-ranges", ""}, {"accept", ""},
        {"access-control-allow-origin", ""}, {"age", ""}, {"allow", ""},
        {"authorization", ""}, {"cache-control", ""},
        {"content-disposition", ""}, {"content-encoding", ""},
        {"content-language", ""}, {"content-length", ""},
        {"content-location", ""}, {"content-range", ""}, {"content-type", ""},
        {"cookie", ""}, {"date", ""}, {"etag", ""}, {"expect", ""},
        {"expires", ""}, {"from", ""}, {"host", ""}, {"if-match", ""},
        {"if-modified-since", ""}, {"if-none-match", ""}, {"if-range", ""},
        {"if-unmodified-since", ""}, {"last-modified", ""}, {"link", ""},
        {"location", ""}, {"max-forwards", ""}, {"proxy-authenticate", ""},
        {"proxy-authorization", ""}, {"range", ""}, {"referer", ""},
        {"refresh", ""}, {"retry-after", ""}, {"server", ""},
        {"set-cookie", ""}, {"strict-transport-security", ""},
        {"transfer-encoding", ""}, {"user-agent", ""}, {"vary", ""},
        {"via", ""}, {"www-authenticate", ""}
      };
      if (v <= 61) {
        return static_table[v - 1];
      } else {
        return dt[v - 62];
      }
    }};

  static auto const extractHeaderInteger{
    [this](uint8_t *buf, uint32_t prefix_num) -> std::pair<uint32_t, uint32_t> {
      uint8_t k = static_cast<uint8_t>(std::pow(2, prefix_num) - 1);
      uint8_t byte = extract8(buf) & k;
      bool is_more = (byte == k);

      uint32_t ret = byte;
      uint32_t num_bytes = 1;

      while (is_more) {
        byte = extract8(buf + num_bytes);
        is_more = (byte > 127);
        ret += byte;
        num_bytes++;
      }

      return std::pair<uint32_t, uint32_t>{ret, num_bytes};
    }};

  static auto const extractHeaderString{
    [this](uint8_t *buf) -> std::pair<std::string, uint32_t> {

      static std::vector<uint16_t> huffman_map = {
        0x0e4, 0x002, 0x168, 0x004, 0x19f, 0x006, 0x1ed, 0x008, 0x285, 0x00a,
        0x108, 0x00c, 0x1fe, 0x00e, 0x0f1, 0x010, 0x10f, 0x012, 0x0fb, 0x014,
        0x016, 0x01d, 0x018, 0x18c, 0x01a, 0x102, 0x000, 0x000, 0x000, 0x215,
        0x01f, 0x21a, 0x021, 0x178, 0x023, 0x297, 0x025, 0x20c, 0x027, 0x2af,
        0x029, 0x02b, 0x038, 0x3a4, 0x02d, 0x402, 0x02f, 0x031, 0x2e5, 0x033,
        0x2dd, 0x035, 0x2d5, 0x000, 0x000, 0x001, 0x307, 0x03a, 0x06d, 0x03c,
        0x37f, 0x03e, 0x3e6, 0x040, 0x443, 0x042, 0x044, 0x078, 0x046, 0x064,
        0x048, 0x052, 0x4fa, 0x04a, 0x04c, 0x04f, 0x000, 0x000, 0x002, 0x000,
        0x000, 0x003, 0x054, 0x05c, 0x056, 0x059, 0x000, 0x000, 0x004, 0x000,
        0x000, 0x005, 0x05e, 0x061, 0x000, 0x000, 0x006, 0x000, 0x000, 0x007,
        0x066, 0x095, 0x068, 0x08a, 0x06a, 0x087, 0x000, 0x000, 0x008, 0x3d4,
        0x06f, 0x071, 0x2fa, 0x4bb, 0x073, 0x075, 0x2f4, 0x000, 0x000, 0x009,
        0x0a7, 0x07a, 0x0d2, 0x07c, 0x2a3, 0x07e, 0x4e5, 0x080, 0x082, 0x0b8,
        0x084, 0x08f, 0x000, 0x000, 0x00a, 0x000, 0x000, 0x00b, 0x08c, 0x092,
        0x000, 0x000, 0x00c, 0x000, 0x000, 0x00d, 0x000, 0x000, 0x00e, 0x097,
        0x09f, 0x099, 0x09c, 0x000, 0x000, 0x00f, 0x000, 0x000, 0x010, 0x0a1,
        0x0a4, 0x000, 0x000, 0x011, 0x000, 0x000, 0x012, 0x0a9, 0x0c0, 0x0ab,
        0x0b3, 0x0ad, 0x0b0, 0x000, 0x000, 0x013, 0x000, 0x000, 0x014, 0x0b5,
        0x0bd, 0x000, 0x000, 0x015, 0x0ba, 0x500, 0x000, 0x000, 0x016, 0x000,
        0x000, 0x017, 0x0c2, 0x0ca, 0x0c4, 0x0c7, 0x000, 0x000, 0x018, 0x000,
        0x000, 0x019, 0x0cc, 0x0cf, 0x000, 0x000, 0x01a, 0x000, 0x000, 0x01b,
        0x0d4, 0x0dc, 0x0d6, 0x0d9, 0x000, 0x000, 0x01c, 0x000, 0x000, 0x01d,
        0x0de, 0x0e1, 0x000, 0x000, 0x01e, 0x000, 0x000, 0x01f, 0x13a, 0x0e6,
        0x0e8, 0x131, 0x277, 0x0ea, 0x0ec, 0x129, 0x0ee, 0x105, 0x000, 0x000,
        0x020, 0x0f3, 0x116, 0x0f5, 0x0f8, 0x000, 0x000, 0x021, 0x000, 0x000,
        0x022, 0x29a, 0x0fd, 0x0ff, 0x186, 0x000, 0x000, 0x023, 0x000, 0x000,
        0x024, 0x000, 0x000, 0x025, 0x10a, 0x124, 0x10c, 0x11e, 0x000, 0x000,
        0x026, 0x189, 0x111, 0x113, 0x121, 0x000, 0x000, 0x027, 0x118, 0x11b,
        0x000, 0x000, 0x028, 0x000, 0x000, 0x029, 0x000, 0x000, 0x02a, 0x000,
        0x000, 0x02b, 0x126, 0x175, 0x000, 0x000, 0x02c, 0x12b, 0x12e, 0x000,
        0x000, 0x02d, 0x000, 0x000, 0x02e, 0x133, 0x156, 0x135, 0x14e, 0x137,
        0x14b, 0x000, 0x000, 0x02f, 0x13c, 0x22d, 0x13e, 0x146, 0x140, 0x143,
        0x000, 0x000, 0x030, 0x000, 0x000, 0x031, 0x148, 0x227, 0x000, 0x000,
        0x032, 0x000, 0x000, 0x033, 0x150, 0x153, 0x000, 0x000, 0x034, 0x000,
        0x000, 0x035, 0x158, 0x160, 0x15a, 0x15d, 0x000, 0x000, 0x036, 0x000,
        0x000, 0x037, 0x162, 0x165, 0x000, 0x000, 0x038, 0x000, 0x000, 0x039,
        0x17d, 0x16a, 0x258, 0x16c, 0x272, 0x16e, 0x170, 0x197, 0x172, 0x194,
        0x000, 0x000, 0x03a, 0x000, 0x000, 0x03b, 0x17a, 0x224, 0x000, 0x000,
        0x03c, 0x17f, 0x234, 0x181, 0x21f, 0x183, 0x191, 0x000, 0x000, 0x03d,
        0x000, 0x000, 0x03e, 0x000, 0x000, 0x03f, 0x18e, 0x209, 0x000, 0x000,
        0x040, 0x000, 0x000, 0x041, 0x000, 0x000, 0x042, 0x199, 0x19c, 0x000,
        0x000, 0x043, 0x000, 0x000, 0x044, 0x1a1, 0x1c7, 0x1a3, 0x1b5, 0x1a5,
        0x1ad, 0x1a7, 0x1aa, 0x000, 0x000, 0x045, 0x000, 0x000, 0x046, 0x1af,
        0x1b2, 0x000, 0x000, 0x047, 0x000, 0x000, 0x048, 0x1b7, 0x1bf, 0x1b9,
        0x1bc, 0x000, 0x000, 0x049, 0x000, 0x000, 0x04a, 0x1c1, 0x1c4, 0x000,
        0x000, 0x04b, 0x000, 0x000, 0x04c, 0x1c9, 0x1db, 0x1cb, 0x1d3, 0x1cd,
        0x1d0, 0x000, 0x000, 0x04d, 0x000, 0x000, 0x04e, 0x1d5, 0x1d8, 0x000,
        0x000, 0x04f, 0x000, 0x000, 0x050, 0x1dd, 0x1e5, 0x1df, 0x1e2, 0x000,
        0x000, 0x051, 0x000, 0x000, 0x052, 0x1e7, 0x1ea, 0x000, 0x000, 0x053,
        0x000, 0x000, 0x054, 0x1ef, 0x24e, 0x1f1, 0x1f9, 0x1f3, 0x1f6, 0x000,
        0x000, 0x055, 0x000, 0x000, 0x056, 0x1fb, 0x203, 0x000, 0x000, 0x057,
        0x200, 0x206, 0x000, 0x000, 0x058, 0x000, 0x000, 0x059, 0x000, 0x000,
        0x05a, 0x000, 0x000, 0x05b, 0x20e, 0x2bf, 0x210, 0x2a8, 0x212, 0x3f7,
        0x000, 0x000, 0x05c, 0x217, 0x2a0, 0x000, 0x000, 0x05d, 0x21c, 0x29d,
        0x000, 0x000, 0x05e, 0x221, 0x22a, 0x000, 0x000, 0x05f, 0x000, 0x000,
        0x060, 0x000, 0x000, 0x061, 0x000, 0x000, 0x062, 0x22f, 0x249, 0x231,
        0x23b, 0x000, 0x000, 0x063, 0x236, 0x241, 0x238, 0x23e, 0x000, 0x000,
        0x064, 0x000, 0x000, 0x065, 0x000, 0x000, 0x066, 0x243, 0x246, 0x000,
        0x000, 0x067, 0x000, 0x000, 0x068, 0x24b, 0x267, 0x000, 0x000, 0x069,
        0x250, 0x26d, 0x252, 0x255, 0x000, 0x000, 0x06a, 0x000, 0x000, 0x06b,
        0x25a, 0x262, 0x25c, 0x25f, 0x000, 0x000, 0x06c, 0x000, 0x000, 0x06d,
        0x264, 0x26a, 0x000, 0x000, 0x06e, 0x000, 0x000, 0x06f, 0x000, 0x000,
        0x070, 0x26f, 0x282, 0x000, 0x000, 0x071, 0x274, 0x27f, 0x000, 0x000,
        0x072, 0x279, 0x27c, 0x000, 0x000, 0x073, 0x000, 0x000, 0x074, 0x000,
        0x000, 0x075, 0x000, 0x000, 0x076, 0x287, 0x28f, 0x289, 0x28c, 0x000,
        0x000, 0x077, 0x000, 0x000, 0x078, 0x291, 0x294, 0x000, 0x000, 0x079,
        0x000, 0x000, 0x07a, 0x000, 0x000, 0x07b, 0x000, 0x000, 0x07c, 0x000,
        0x000, 0x07d, 0x000, 0x000, 0x07e, 0x2a5, 0x472, 0x000, 0x000, 0x07f,
        0x438, 0x2aa, 0x2ac, 0x2bc, 0x000, 0x000, 0x080, 0x398, 0x2b1, 0x2b3,
        0x332, 0x2b5, 0x2cb, 0x498, 0x2b7, 0x2b9, 0x2c8, 0x000, 0x000, 0x081,
        0x000, 0x000, 0x082, 0x2c1, 0x329, 0x2c3, 0x3c0, 0x2c5, 0x354, 0x000,
        0x000, 0x083, 0x000, 0x000, 0x084, 0x2cd, 0x2d8, 0x2cf, 0x2d2, 0x000,
        0x000, 0x085, 0x000, 0x000, 0x086, 0x000, 0x000, 0x087, 0x2da, 0x304,
        0x000, 0x000, 0x088, 0x2df, 0x2e2, 0x000, 0x000, 0x089, 0x000, 0x000,
        0x08a, 0x2e7, 0x2ef, 0x2e9, 0x2ec, 0x000, 0x000, 0x08b, 0x000, 0x000,
        0x08c, 0x2f1, 0x2f7, 0x000, 0x000, 0x08d, 0x000, 0x000, 0x08e, 0x000,
        0x000, 0x08f, 0x2fc, 0x312, 0x2fe, 0x301, 0x000, 0x000, 0x090, 0x000,
        0x000, 0x091, 0x000, 0x000, 0x092, 0x309, 0x361, 0x30b, 0x322, 0x30d,
        0x31a, 0x30f, 0x317, 0x000, 0x000, 0x093, 0x314, 0x349, 0x000, 0x000,
        0x094, 0x000, 0x000, 0x095, 0x31c, 0x31f, 0x000, 0x000, 0x096, 0x000,
        0x000, 0x097, 0x324, 0x341, 0x326, 0x33b, 0x000, 0x000, 0x098, 0x482,
        0x32b, 0x32d, 0x36d, 0x32f, 0x351, 0x000, 0x000, 0x099, 0x334, 0x35a,
        0x336, 0x34c, 0x338, 0x33e, 0x000, 0x000, 0x09a, 0x000, 0x000, 0x09b,
        0x000, 0x000, 0x09c, 0x343, 0x346, 0x000, 0x000, 0x09d, 0x000, 0x000,
        0x09e, 0x000, 0x000, 0x09f, 0x34e, 0x357, 0x000, 0x000, 0x0a0, 0x000,
        0x000, 0x0a1, 0x000, 0x000, 0x0a2, 0x000, 0x000, 0x0a3, 0x35c, 0x37a,
        0x35e, 0x377, 0x000, 0x000, 0x0a4, 0x363, 0x391, 0x365, 0x372, 0x367,
        0x36a, 0x000, 0x000, 0x0a5, 0x000, 0x000, 0x0a6, 0x36f, 0x388, 0x000,
        0x000, 0x0a7, 0x374, 0x38e, 0x000, 0x000, 0x0a8, 0x000, 0x000, 0x0a9,
        0x37c, 0x38b, 0x000, 0x000, 0x0aa, 0x381, 0x409, 0x383, 0x459, 0x385,
        0x432, 0x000, 0x000, 0x0ab, 0x000, 0x000, 0x0ac, 0x000, 0x000, 0x0ad,
        0x000, 0x000, 0x0ae, 0x393, 0x3b8, 0x395, 0x3b2, 0x000, 0x000, 0x0af,
        0x39a, 0x45e, 0x39c, 0x3ad, 0x39e, 0x3a1, 0x000, 0x000, 0x0b0, 0x000,
        0x000, 0x0b1, 0x3a6, 0x3cd, 0x3a8, 0x3c5, 0x3aa, 0x3b5, 0x000, 0x000,
        0x0b2, 0x3af, 0x43b, 0x000, 0x000, 0x0b3, 0x000, 0x000, 0x0b4, 0x000,
        0x000, 0x0b5, 0x3ba, 0x3bd, 0x000, 0x000, 0x0b6, 0x000, 0x000, 0x0b7,
        0x3c2, 0x3f4, 0x000, 0x000, 0x0b8, 0x3c7, 0x3ca, 0x000, 0x000, 0x0b9,
        0x000, 0x000, 0x0ba, 0x3cf, 0x3de, 0x3d1, 0x3db, 0x000, 0x000, 0x0bb,
        0x3d6, 0x3fd, 0x3d8, 0x3e3, 0x000, 0x000, 0x0bc, 0x000, 0x000, 0x0bd,
        0x3e0, 0x3fa, 0x000, 0x000, 0x0be, 0x000, 0x000, 0x0bf, 0x3e8, 0x421,
        0x3ea, 0x41a, 0x3ec, 0x412, 0x3ee, 0x3f1, 0x000, 0x000, 0x0c0, 0x000,
        0x000, 0x0c1, 0x000, 0x000, 0x0c2, 0x000, 0x000, 0x0c3, 0x000, 0x000,
        0x0c4, 0x3ff, 0x49b, 0x000, 0x000, 0x0c5, 0x404, 0x49e, 0x406, 0x492,
        0x000, 0x000, 0x0c6, 0x4ae, 0x40b, 0x40d, 0x4a6, 0x40f, 0x435, 0x000,
        0x000, 0x0c7, 0x414, 0x417, 0x000, 0x000, 0x0c8, 0x000, 0x000, 0x0c9,
        0x41c, 0x43e, 0x41e, 0x42f, 0x000, 0x000, 0x0ca, 0x468, 0x423, 0x4c6,
        0x425, 0x4fd, 0x427, 0x429, 0x42c, 0x000, 0x000, 0x0cb, 0x000, 0x000,
        0x0cc, 0x000, 0x000, 0x0cd, 0x000, 0x000, 0x0ce, 0x000, 0x000, 0x0cf,
        0x000, 0x000, 0x0d0, 0x000, 0x000, 0x0d1, 0x440, 0x451, 0x000, 0x000,
        0x0d2, 0x445, 0x4d1, 0x447, 0x478, 0x449, 0x454, 0x44b, 0x44e, 0x000,
        0x000, 0x0d3, 0x000, 0x000, 0x0d4, 0x000, 0x000, 0x0d5, 0x456, 0x475,
        0x000, 0x000, 0x0d6, 0x45b, 0x487, 0x000, 0x000, 0x0d7, 0x460, 0x48d,
        0x462, 0x465, 0x000, 0x000, 0x0d8, 0x000, 0x000, 0x0d9, 0x46a, 0x4b6,
        0x46c, 0x46f, 0x000, 0x000, 0x0da, 0x000, 0x000, 0x0db, 0x000, 0x000,
        0x0dc, 0x000, 0x000, 0x0dd, 0x47a, 0x4c1, 0x47c, 0x47f, 0x000, 0x000,
        0x0de, 0x000, 0x000, 0x0df, 0x484, 0x48a, 0x000, 0x000, 0x0e0, 0x000,
        0x000, 0x0e1, 0x000, 0x000, 0x0e2, 0x48f, 0x495, 0x000, 0x000, 0x0e3,
        0x000, 0x000, 0x0e4, 0x000, 0x000, 0x0e5, 0x000, 0x000, 0x0e6, 0x000,
        0x000, 0x0e7, 0x4a0, 0x4a3, 0x000, 0x000, 0x0e8, 0x000, 0x000, 0x0e9,
        0x4a8, 0x4ab, 0x000, 0x000, 0x0ea, 0x000, 0x000, 0x0eb, 0x4b0, 0x4b3,
        0x000, 0x000, 0x0ec, 0x000, 0x000, 0x0ed, 0x4b8, 0x4be, 0x000, 0x000,
        0x0ee, 0x000, 0x000, 0x0ef, 0x000, 0x000, 0x0f0, 0x4c3, 0x4ce, 0x000,
        0x000, 0x0f1, 0x4c8, 0x4cb, 0x000, 0x000, 0x0f2, 0x000, 0x000, 0x0f3,
        0x000, 0x000, 0x0f4, 0x4d3, 0x4e8, 0x4d5, 0x4dd, 0x4d7, 0x4da, 0x000,
        0x000, 0x0f5, 0x000, 0x000, 0x0f6, 0x4df, 0x4e2, 0x000, 0x000, 0x0f7,
        0x000, 0x000, 0x0f8, 0x000, 0x000, 0x0f9, 0x4ea, 0x4f2, 0x4ec, 0x4ef,
        0x000, 0x000, 0x0fa, 0x000, 0x000, 0x0fb, 0x4f4, 0x4f7, 0x000, 0x000,
        0x0fc, 0x000, 0x000, 0x0fd, 0x000, 0x000, 0x0fe, 0x000, 0x000, 0x0ff,
        0x000, 0x000, 0x100
      };

      static auto const traverseHuffmanMap{
        [](uint16_t &pos, bool next) -> int16_t {
          pos = huffman_map[pos + next];
          if (!huffman_map[pos] && !huffman_map[pos+1]) {
            int16_t v = huffman_map[pos+2];
            pos = 0;
            if (v == 256) {
              return -2;
            }
            return v;
          }
          return -1;
        }};

      std::pair<uint32_t, uint32_t> length = extractHeaderInteger(buf, 7);

      uint8_t byte = extract8(buf);
      bool isHuffman = byte & 0x80;

      std::string ret;
      uint32_t num_bytes = length.first + length.second;

      uint16_t pos = 0;
      for (uint32_t i = 0; i < length.first; ++i) {
        uint8_t data = extract8(buf + i + length.second);
        if (isHuffman) {
          // TODO: check endian before?
          for (int32_t j = 7; j >= 0; --j) {

            uint8_t mask = static_cast<uint8_t>(std::pow(2, j));
            uint8_t bit = (data & mask) >> j;

            int16_t r = traverseHuffmanMap(pos,
                static_cast<bool>(bit));
            // TODO: How to handle EOS (r == -2)?
            if (r != -1) {
              ret.push_back(static_cast<char>(r));
            }
          }
        } else {
          ret.push_back(static_cast<char>(data));
        }
      }

      return std::pair<std::string, uint32_t>{ret, num_bytes};
    }};

  /*
  std::cout << "Dynamic table before" << std::endl;
  for (auto &f : dynamic_table) {
    std::cout << "  " << f.first << ": " << f.second << std::endl;
  }
  */

  uint32_t length = extract24(m_buf_pos);

  uint8_t flags = extract8(m_buf_pos + 4);
  bool is_end_stream = static_cast<bool>(flags & 0x1);
  bool is_end_headers = static_cast<bool>(flags & 0x4);
  bool is_padded = static_cast<bool>(flags & 0x8);
  bool is_priority = static_cast<bool>(flags & 0x20);

  /*
  std::cout << "  ... flags: is_end_stream " << is_end_stream
    << ", is_end_header " << is_end_headers
    << ", is_padded " << is_padded
    << ", is_priority " << is_priority << std::endl;
    */

  uint32_t stream_id = extract32(m_buf_pos + 5);

  uint8_t pad_length{0};

  uint32_t offset{0};
  if (is_padded) {
    pad_length = extract8(m_buf_pos + 9);
    offset++;
  }

  if (is_priority) {
    std::pair<uint32_t, bool> dep_stream_id_and_exclusive_bit =
      extract31WithFirstBitFlag(m_buf_pos + 9 + offset);
    uint8_t weight = extract8(m_buf_pos + 13 + offset);
    if (m_priority_delegate != nullptr) {
      m_priority_delegate(stream_id, dep_stream_id_and_exclusive_bit.second,
          dep_stream_id_and_exclusive_bit.first, weight);
    }
    offset += 5;
  }

  std::map<std::string, std::string> fields;

  uint8_t *buf_end = m_buf_pos + 9 + length;
  uint8_t *pos = m_buf_pos + 9 + offset;
  while (pos < buf_end) {
    uint8_t const next = extract8(pos);
    //std::cout << "next byte " << std::bitset<8>(next) << std::hex
    //  << " (" << +next << ")" << std::dec << std::endl;

    bool read_index = false;
    bool add_dynamic = false;
    bool name_literal = false;
    bool field_literal = false;
    bool never_index = false;
    uint32_t index_prefix_len = 0;

    std::string name;
    std::string field;

    if (next & 0x80) {
      read_index = true;
      index_prefix_len = 7;
    } else if (next & 0x40) {
      if (next == 0x40) {
        add_dynamic = true;
        name_literal = true;
        field_literal = true;
        pos++;
      } else {
        add_dynamic = true;
        read_index = true;
        index_prefix_len = 6;
        field_literal = true;
      }
    } else if (next < 0x10) {
      if (next == 0x0) {
        name_literal = true;
        field_literal = true;
        pos++;
      } else {
        read_index = true;
        index_prefix_len = 4;
        field_literal = true;
      }
    } else {
      if (next == 0x10) {
        name_literal = true;
        field_literal = true;
        never_index = true;
        pos++;
      } else {
        read_index = true;
        index_prefix_len = 4;
        field_literal = true;
        never_index = true;
      }
    }

    if (read_index) {
      std::pair<uint32_t, uint32_t> index = extractHeaderInteger(pos,
          index_prefix_len);
      pos += index.second;

      auto pair = header_table(dynamic_table, index.first);
      name = pair.first;
      field = pair.second;

      /*
      std::cout << "idx " << index.first << std::endl;
      std::cout << "name " << name << std::endl;
      std::cout << "value " << field << std::endl;
      */
    }
    if (name_literal) {
      auto str = extractHeaderString(pos);
      pos += str.second;
      name = str.first;
      //std::cout << "name " << name << std::endl;
    }
    if (field_literal) {
      auto str = extractHeaderString(pos);
      pos += str.second;
      field = str.first;
      //std::cout << "value " << field << std::endl;
    }

    if (add_dynamic) {
      if (max_header_list_size == 0) {
        dynamic_table.insert(dynamic_table.begin(), {name, field});
      } else {
        uint32_t entry_size = name.length() + field.length() + 32;
        uint32_t current_size{0};
        std::vector<uint32_t> sizes;
        for (auto &e : dynamic_table) {
          uint32_t size = e.first.length() + e.second.length() + 32;
          current_size += size;
          sizes.push_back(size);
        }

        if (entry_size <= max_header_list_size) {
          while (current_size + entry_size > max_header_list_size) {
            current_size -= sizes.back();
            dynamic_table.pop_back();
            sizes.pop_back();
          }
          dynamic_table.insert(dynamic_table.begin(), {name, field});
        } else {
          dynamic_table.clear();
        }
      }
    }
    if (never_index) {
      //std::cout << "TODO: Never index: " << name << " : " << field
      //<< std::endl;
    }

    fields[name] = field;
  }

  (void) pad_length;
  // TODO: In the end, check that we have the right number of padding bytes,
  // all zero. Or give PROTOCOL_ERROR

  /*
  std::cout << "Dynamic table after" << std::endl;
  for (auto &f : dynamic_table) {
    std::cout << "  " << f.first << ": " << f.second << std::endl;
  }
  */

  if (m_header_delegate != nullptr) {
    m_header_delegate(stream_id, is_end_stream, is_end_headers, fields);
  }

  m_buf_pos += 9 + length;

  return 1;
}

inline int32_t Http2Decoder::decodePing() noexcept
{
  if (m_ping_delegate != nullptr) {
    uint8_t flags = *(m_buf_pos + 4);
    bool is_ack = flags & 0x1;
    uint64_t opaque_data = extract64(m_buf_pos + 5);
    m_ping_delegate(opaque_data, is_ack);
  }
  m_buf_pos += 17;
  return 1;
}

inline int32_t Http2Decoder::decodePreface() noexcept
{
  if (m_preface_delegate != nullptr) {
    uint32_t w0 = extract32(m_buf_pos);
    uint32_t w5 = extract32(m_buf_pos + 20);
    if (w0 != 1347569952 || w5 != 218762506) {
      return -1;
    }
    m_preface_delegate();
  }
  m_buf_pos += 24;
  return 1;
}

inline int32_t Http2Decoder::decodePriority() noexcept
{
  if (m_priority_delegate != nullptr) {
    uint32_t stream_id = extract32(m_buf_pos + 5);
    std::pair<uint32_t, bool> dep_stream_id_and_exclusive_bit
      = extract31WithFirstBitFlag(m_buf_pos + 9);
    uint8_t weight = (extract8(m_buf_pos + 13) + 1);
    m_priority_delegate(stream_id, dep_stream_id_and_exclusive_bit.second,
        dep_stream_id_and_exclusive_bit.first, weight);
  }
  m_buf_pos += 14;
  return 1;
}

inline int32_t Http2Decoder::decodeRstStream() noexcept
{
  if (m_rst_stream_delegate != nullptr) {
    uint32_t stream_id = extract32(m_buf_pos + 5);
    uint32_t error_code = extract32(m_buf_pos + 9);
    m_rst_stream_delegate(stream_id, error_code);
  }
  m_buf_pos += 13;
  return 1;
}

inline int32_t Http2Decoder::decodeSettings() noexcept
{
  uint8_t flags = *(m_buf_pos + 4);
  switch (flags) {
    case 0:
      {
        int32_t length = extract24(m_buf_pos);
        uint32_t parameters = length / 6;
        if (m_settings_delegate != nullptr) {
          std::map<uint8_t, uint32_t> settings;
          for (uint32_t i = 0; i < parameters; ++i) {
            uint8_t id = extract8(m_buf_pos + 10 + 6 * i);
            uint32_t value = extract32(m_buf_pos + 11 + 6 * i);
            if (id == 2 && value != 1 && value != 0) {
              return -1;
            }
            settings[id] = value;
          }
          m_settings_delegate(settings);
        }
        m_buf_pos += 9 + parameters * 6;
        return 1;
      }
    case 1:
      if (m_settings_ack_delegate != nullptr) {
        m_settings_ack_delegate();
      }
      m_buf_pos += 9;
      return 1;
    default:
      return -1;
  };
}

inline int32_t Http2Decoder::decodeWindowUpdate() noexcept
{
  if (m_window_update_delegate != nullptr) {
    uint32_t stream_id = extract32(m_buf_pos + 5);
    std::pair<uint32_t, bool> window_size_and_reserved_bit
      = extract31WithFirstBitFlag(m_buf_pos + 9);
    m_window_update_delegate(stream_id, window_size_and_reserved_bit.first);
  }
  m_buf_pos += 13;
  return 1;
}

inline uint8_t Http2Decoder::extract8(uint8_t *buf_pos) const noexcept
{
  return *buf_pos;
}

inline uint32_t Http2Decoder::extract24(uint8_t *buf_pos) const noexcept
{
  uint32_t d_n;
  ::memcpy(&d_n, buf_pos, 3);
  return ntohl(d_n << 8);
}

inline std::pair<uint32_t, bool>
Http2Decoder::extract31WithFirstBitFlag(uint8_t *buf_pos) const noexcept
{
  uint32_t d = extract32(buf_pos);
  bool b = (*buf_pos & 0x80) > 0;
  return std::pair<uint32_t, bool>(d, b);
}

inline uint32_t Http2Decoder::extract32(uint8_t *buf_pos) const noexcept
{
  uint32_t d_n;
  ::memcpy(&d_n, buf_pos, 4);
  return ntohl(d_n);
}

inline uint64_t Http2Decoder::extract64(uint8_t *buf_pos) const noexcept
{
  uint64_t d_n;
  ::memcpy(&d_n, buf_pos, 8);
  return ntohl(d_n);
}

inline std::string Http2Decoder::extractString(uint8_t *buf_pos,
    uint32_t length) const noexcept
{
  if (length <= 0) {
    return std::string();
  }
  std::string str(length, ' ');
  ::memcpy(str.data(), buf_pos, length);
  return str;
}

inline uint32_t Http2Decoder::maxLength() const noexcept
{
  return m_buf_end - m_buf_start;
}

inline uint32_t Http2Decoder::frameLength(uint32_t length) const noexcept
{
  if (length >= 8) {
    if (extract64(m_buf_pos) == 0x50524920) {
      if (length > 24) {
        return 24 + extract24(m_buf_pos + 24) + 9;
      } else {
        return 24;
      }
    } else {
      return extract24(m_buf_pos) + 9;
    }
  } else {
    return 0;
  }
}

inline void Http2Decoder::onData(
    std::function<void(uint32_t, bool, std::string)> data_delegate) noexcept
{
  m_data_delegate = data_delegate;
}

inline void Http2Decoder::onGoAway(
    std::function<void(uint32_t, uint32_t, std::string)> go_away_delegate)
  noexcept
{
  m_go_away_delegate = go_away_delegate;
}

inline void Http2Decoder::onHeader(
    std::function<void(uint32_t, bool, bool,
      std::map<std::string, std::string>)> header_delegate) noexcept
{
  m_header_delegate = header_delegate;
}

inline void Http2Decoder::onPing(
    std::function<void(uint64_t, bool)> ping_delegate) noexcept
{
  m_ping_delegate = ping_delegate;
}

inline void Http2Decoder::onPriority(
    std::function<void(uint32_t, bool, uint32_t, uint8_t)> priority_delegate)
  noexcept
{
  m_priority_delegate = priority_delegate;
}

inline void Http2Decoder::onPreface(std::function<void()> preface_delegate)
  noexcept
{
  m_preface_delegate = preface_delegate;
}

inline void Http2Decoder::onRstStream(
    std::function<void(uint32_t, uint32_t)> rst_stream_delegate) noexcept
{
  m_rst_stream_delegate = rst_stream_delegate;
}

inline void Http2Decoder::onSettingsAck(
    std::function<void()> settings_ack_delegate) noexcept
{
  m_settings_ack_delegate = settings_ack_delegate;
}

inline void Http2Decoder::onSettings(
    std::function<void(std::map<uint8_t, uint32_t>)> settings_delegate) noexcept
{
  m_settings_delegate = settings_delegate;
}

inline void Http2Decoder::onWindowUpdate(
    std::function<void(uint32_t, uint32_t)> window_update_delegate) noexcept
{
  m_window_update_delegate = window_update_delegate;
}

inline void Http2Decoder::reset() noexcept
{
  m_buf_pos = m_buf_start;
}

class Server {
  public:
    Server(Verbosity = Verbosity::ON_ERROR) noexcept;
    Server(Server const &) = delete;
    Server(Server &&) = delete;
    ~Server() = default;

    Server &operator=(Server const &) = delete;
    Server &operator=(Server &&) = delete;

    bool isRunning() const;
    int32_t run(std::string, std::string, uint32_t, bool, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<void(std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::function<void(std::string,
            uint32_t, std::shared_ptr<std::string const>)>,
          std::function<void(std::string, void *, std::string const &)>)>)
      noexcept;
    int32_t run(std::string, std::string, std::string, uint32_t, bool, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<void(std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::function<void(std::string,
            uint32_t, std::shared_ptr<std::string const>)>,
          std::function<void(std::string, void *, std::string const &)>)>)
      noexcept;
    int32_t run(std::string, std::string, uint32_t, bool, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<void(std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::string const &,
          std::function<void(std::string, uint32_t,
            std::shared_ptr<std::string const>)>,
          std::function<void(std::string, void *, std::string const &)>)>)
      noexcept;
    int32_t run(std::string, std::string, std::string, uint32_t, bool, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<void(std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::string const &,
          std::function<void(std::string, uint32_t,
            std::shared_ptr<std::string const>)>,
          std::function<void(std::string, void *, std::string const &)>)>)
      noexcept;
    void sendEvents(std::string, std::function<std::string(void *)>)
      noexcept;
    void stop() noexcept;

  private:
    struct Stream {
      enum class State {
        Open,
        HalfClosed,
        Closed
      };

      State state;
      std::map<std::string, std::string> fields;
      std::shared_ptr<std::string> data_in;
      std::shared_ptr<std::string const> data_out;
      std::mutex stream_mutex;
      uint32_t data_in_pos;
      uint32_t data_out_pos;
      int32_t window_size_in;
      int32_t window_size_out;
      bool is_full_new_request;
      bool is_server_side_event;

      Stream(uint32_t initial_window_size_out) : state(State::Open), fields(),
      data_in(), data_out(), stream_mutex(), data_in_pos(0), data_out_pos(0),
      window_size_in(0), window_size_out(initial_window_size_out),
      is_full_new_request(false), is_server_side_event(false) {}
      Stream() : Stream(0) {}

      Stream(Stream const &) = delete;
      Stream(Stream &&) = delete;

      Stream &operator=(Stream const &) = delete;
      Stream &operator=(Stream &&) = delete;
    };

    struct Session {
      std::map<uint32_t, Stream> streams;
      std::vector<std::pair<std::string, std::string>> dynamic_table;
      std::mutex write_mutex;
      std::shared_ptr<SSL> ssl;
      std::string client_addr;
      uint32_t header_table_size;
      uint32_t initial_stream_window_size;
      uint32_t max_concurrent_streams;
      uint32_t max_header_list_size;
      uint32_t max_frame_size;
      int32_t window_size;
      bool is_handshaked;

      Session(std::shared_ptr<SSL> a_ssl, std::string a_client_addr) :
        streams(), dynamic_table(), write_mutex(),
        ssl(a_ssl), client_addr(a_client_addr), header_table_size(4096),
        initial_stream_window_size(65535), max_concurrent_streams(0),
        max_header_list_size(0), max_frame_size(16384), window_size(65535),
        is_handshaked(false) {}

      Session(Session const &) = delete;
      Session(Session &&) = delete;

      Session &operator=(Session const &) = delete;
      Session &operator=(Session &&) = delete;
    };

    struct EventData {
      enum class SocketType {
        Listen,
        Client
      };
      std::unique_ptr<Session> session;
      SocketType socket_type;
      std::mutex session_mutex;
      int32_t fd_remote;
      int32_t fd_local;

      EventData(int32_t a_fd_remote) :
        session(nullptr), socket_type(SocketType::Listen), session_mutex(), 
        fd_remote(a_fd_remote), fd_local(-1) {}
      EventData(int32_t a_fd_remote, int32_t a_fd_local,
          std::unique_ptr<Session> a_session) :
        session(std::move(a_session)), socket_type(SocketType::Client),
        session_mutex(), fd_remote(a_fd_remote), fd_local(a_fd_local) {}

      EventData(EventData const &) = delete;
      EventData(EventData &&) = delete;

      EventData &operator=(EventData const &) = delete;
      EventData &operator=(EventData &&) = delete;
    };

#ifdef TINYH2_DEBUG
    std::mutex m_debug_mutex;
    std::string m_debug_socketinfo;
#endif
    std::atomic<bool> m_is_running;
    std::map<std::string, std::map<EventData *, std::map<uint32_t,
      void *>>> m_event_sources;
    uint32_t m_verbosity;
};

inline Server::Server(Verbosity verbosity) noexcept:
#ifdef TINYH2_DEBUG
  m_debug_mutex(),
  m_debug_socketinfo(),
#endif
  m_is_running(false),
  m_event_sources(),
  m_verbosity(verbosity)
{
}

inline bool Server::isRunning() const
{
  return m_is_running.load();
}

inline int32_t Server::run(std::string private_key_path,
    std::string public_key_path, uint32_t port, bool do_support_sse,
    uint32_t worker_count, std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<void(std::map<std::string, std::string>,
      std::shared_ptr<std::string>, std::function<void(std::string, uint32_t, 
        std::shared_ptr<std::string const>)>, std::function<void(std::string,
          void *, std::string const &)>)> request_delegate) noexcept
{
  return run(private_key_path, public_key_path, "", port, do_support_sse,
      worker_count, prepare_incoming_delegate, append_incoming_delegate,
      request_delegate);
}

inline int32_t Server::run(std::string private_key_path,
    std::string public_key_path, std::string address, uint32_t port,
    bool do_support_sse, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<void(std::map<std::string, std::string>,
      std::shared_ptr<std::string>, std::function<void(std::string, uint32_t, 
        std::shared_ptr<std::string const>)>, std::function<void(std::string,
          void *, std::string const &)>)> request_delegate) noexcept
{
  auto request_delegate_2{
    [&request_delegate](std::map<std::string, std::string> fields, 
        std::shared_ptr<std::string> data_in, std::string const &,
        std::function<void(std::string, uint32_t,
          std::shared_ptr<std::string const>)> send_response,
        std::function<void(std::string, void *,
          std::string const &)> register_for_event_source) {
      request_delegate(fields, data_in, send_response,
          register_for_event_source);
    }};
  return run(private_key_path, public_key_path, address, port, do_support_sse,
      worker_count, prepare_incoming_delegate, append_incoming_delegate,
      request_delegate_2);
}

inline int32_t Server::run(std::string private_key_path,
    std::string public_key_path, uint32_t port, bool do_support_sse,
    uint32_t worker_count, std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<void(std::map<std::string, std::string>,
      std::shared_ptr<std::string>, std::string const &,
      std::function<void(std::string, uint32_t, 
        std::shared_ptr<std::string const>)>, std::function<void(std::string,
          void *, std::string const &)>)> request_delegate) noexcept
{
  return run(private_key_path, public_key_path, "", port, do_support_sse,
      worker_count, prepare_incoming_delegate, append_incoming_delegate,
      request_delegate);
}

inline int32_t Server::run(std::string private_key_path,
    std::string public_key_path, std::string address, uint32_t port,
    bool do_support_sse, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<void(std::map<std::string, std::string>,
      std::shared_ptr<std::string>, std::string const &,
      std::function<void(std::string, uint32_t, 
        std::shared_ptr<std::string const>)>, std::function<void(std::string,
          void *, std::string const &)>)> request_delegate) noexcept
{

  struct sigaction sa;
  ::memset(&sa, 0, sizeof(sa));
  sa.sa_handler = gotSignal;
  ::sigfillset(&sa.sa_mask);
  ::sigaction(SIGINT, &sa, nullptr);

  sigset_t set;
  ::sigemptyset(&set);
  ::sigaddset(&set, SIGPIPE);
  ::pthread_sigmask(SIG_BLOCK, &set, nullptr);

  // TODO: Only when verbosity is high.
  SSL_load_error_strings();
  ERR_load_crypto_strings();

  ERR_clear_error();

  std::unique_ptr<SSL_CTX, std::function<void(SSL_CTX *)>> ssl_ctx(
      SSL_CTX_new(TLS_server_method()), [this](SSL_CTX *a_ssl_ctx) {
        if (m_verbosity >= Verbosity::INFO) {
          std::clog << "Deleting SSL context" << std::endl;
        }
        SSL_CTX_free(a_ssl_ctx);
      });
  if (!ssl_ctx) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Could not create SSL context, err="
        << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
    }
    return -1;
  }

  SSL_CTX_set_options(&(*ssl_ctx),
      (SSL_OP_ALL & ~SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS)
      | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION
      | SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION | SSL_OP_SINGLE_ECDH_USE
      | SSL_OP_NO_TICKET | SSL_OP_CIPHER_SERVER_PREFERENCE);

  SSL_CTX_set_mode(&(*ssl_ctx), SSL_MODE_AUTO_RETRY);
  SSL_CTX_set_mode(&(*ssl_ctx), SSL_MODE_RELEASE_BUFFERS);

  SSL_CTX_set_min_proto_version(&(*ssl_ctx), TLS1_VERSION);
  SSL_CTX_set_max_proto_version(&(*ssl_ctx), TLS1_3_VERSION);

  SSL_CTX_set_cipher_list(&(*ssl_ctx), "ECDHE-ECDSA-AES256-GCM-SHA384:"
      "ECDHE-RSA-AES256-GCM-SHA384:"
      "ECDHE-ECDSA-CHACHA20-POLY1305:"
      "ECDHE-RSA-CHACHA20-POLY1305:"
      "ECDHE-ECDSA-AES128-GCM-SHA256:"
      "ECDHE-RSA-AES128-GCM-SHA256:"
      "ECDHE-ECDSA-AES256-SHA384:"
      "ECDHE-RSA-AES256-SHA384:"
      "ECDHE-ECDSA-AES128-SHA256:"
      "ECDHE-RSA-AES128-SHA256");

  unsigned char const sid_ctx[] = "tinyh2";
  SSL_CTX_set_session_id_context(&(*ssl_ctx), sid_ctx,  sizeof(sid_ctx) - 1);

  SSL_CTX_set_session_cache_mode(&(*ssl_ctx), SSL_SESS_CACHE_SERVER);

  {
    int32_t rv = SSL_CTX_use_PrivateKey_file(&(*ssl_ctx),
        private_key_path.c_str(), SSL_FILETYPE_PEM);
    if (!rv) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not set up SSL key, err="
          << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
      }
      return -1;
    }

    rv = SSL_CTX_use_certificate_chain_file(&(*ssl_ctx),
        public_key_path.c_str());
    if (!rv) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not set up SSL certificate, err="
          << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
      }
      return -1;
    }

    rv = SSL_CTX_check_private_key(&(*ssl_ctx));
    if (!rv) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "SSL key error, err=" <<
          ERR_error_string(ERR_get_error(), nullptr) << std::endl;
      }
      return -1;
    }
  }

  /*
   * TODO: Make this work...
   *
  auto verify_ssl_client{[](int32_t, X509_STORE_CTX *) -> int32_t
    {
      return 1;
    }};

  SSL_CTX_set_verify(&(*ssl_ctx), SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE |
      SSL_VERIFY_FAIL_IF_NO_PEER_CERT, verify_ssl_client);
*/
  auto alpn_select_proto{[](SSL *, unsigned char const **out,
      unsigned char *outlen, unsigned char const *in, uint32_t inlen,
      void *) -> int32_t
    {
      uint32_t const h2_len = 2;
      for (uint32_t i = 0; i < inlen - h2_len; ++i) {
        if (in[i] == 2 && in[i+1] == 104 && in[i+2] == 50) {
          *out = (in + i + 1);
          *outlen = h2_len;
          return SSL_TLSEXT_ERR_OK;
        }
      }
      return SSL_TLSEXT_ERR_NOACK;
    }};

  SSL_CTX_set_alpn_select_cb(&(*ssl_ctx), alpn_select_proto, nullptr);

  struct addrinfo hints{};
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;

  struct addrinfo *ais;
  {
    int32_t rv = getaddrinfo(!address.empty() ? address.c_str() : nullptr,
        std::to_string(port).c_str(), &hints, &ais);
    if (rv != 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not get address, err=" << gai_strerror(rv)
          << std::endl;
      }
      return -1;
    }
  }

  bool has_listener = false;
  int32_t fd_epoll = epoll_create1(0);
  std::vector<int32_t> fds_listen;

  std::map<int32_t, std::shared_ptr<EventData>> sockets;
  std::mutex sockets_mutex;
  std::atomic<uint32_t> client_count(0);
  std::atomic<uint32_t> job_count(0);

  auto disconnect{[this, &sockets_mutex](
      int32_t fd_remote, int32_t fd_local) {
    ::shutdown(fd_remote, SHUT_RDWR);
    if (fd_remote != -1) {
      ::close(fd_remote);
    }
    if (fd_local != -1) {
      ::close(fd_local);
    }

    std::string const path_local = "p" + std::to_string(fd_remote) + ".ipc";
    ::unlink(path_local.c_str());

    if (m_verbosity >= Verbosity::INFO) {
      std::clog << "Disconnected (bye " << fd_remote << ")" << std::endl;
    }
    fd_remote = -1;
    fd_local = -1;
  }};

  auto close_session{[this, &disconnect, &fd_epoll, &sockets,
    &sockets_mutex](EventData *ed) {
      if (ed) {
        int32_t fd_remote = ed->fd_remote;
        int32_t fd_local = ed->fd_local;

        epoll_ctl(fd_epoll, EPOLL_CTL_DEL, fd_remote, nullptr);
        epoll_ctl(fd_epoll, EPOLL_CTL_DEL, fd_local, nullptr);

        if (ed->session != nullptr) {
          ed->session->ssl.reset();
        }
        
        if (m_verbosity >= Verbosity::INFO) {
          std::clog << "Session closed (" << fd_remote << ")" << std::endl;
        }

        disconnect(fd_remote, fd_local);
      }
    }};

  auto ssl_write{[this](EventData *data, Http2Encoder &encoder) -> bool {
    bool close_connection{false};

    std::lock_guard<std::mutex> const write_lock(data->session->write_mutex);
    if (encoder.length() > 0 && m_verbosity >= Verbosity::DEBUG) {
      std::clog << "Sending data, length=" << encoder.length() << " ("
        << data->fd_remote << ")" << std::endl;

      uint32_t const max_print_len{1600};
      printHex(encoder.data(), std::min(encoder.length(), max_print_len));
      if (encoder.length() > max_print_len) {
        std::clog << "  " << (encoder.length() - max_print_len)
          << " bytes omitted in printout." << std::endl;
      }
    }

    uint64_t bytes_written{0};

    while (encoder.length() > 0) {
      // TODO: Busy waiting until OpenSSL allows write, make better
      // for BearSSL.
      //std::clog << "Will write bytes " << encoder.length() << std::endl;

      int32_t rv = SSL_write(data->session->ssl.get(),
          encoder.data() + bytes_written,
          encoder.length() - bytes_written);

      if (rv <= 0) {
        auto e = SSL_get_error(data->session->ssl.get(), rv);
        if (e == SSL_ERROR_WANT_READ) {
          std::clog << "!!! Should never happen (wants to read)"
            << std::endl;
        } else if (e == SSL_ERROR_WANT_WRITE) {
        } else {
          close_connection = true;
          encoder.reset();
        }
      } else {
        bytes_written += rv;
        if (bytes_written == encoder.length()) {
          encoder.reset();
        }
      }
    }
    return close_connection;
  }};

  for (struct addrinfo *ai = ais; ai != nullptr; ai = ai->ai_next) {

    int32_t fd_listen = ::socket(ai->ai_family, ai->ai_socktype,
        ai->ai_protocol);
    if (fd_listen == -1) {
      continue;
    }

    fds_listen.push_back(fd_listen);

    int32_t flags = ::fcntl(fd_listen, F_GETFL, 0) | O_NONBLOCK;
    ::fcntl(fd_listen, F_SETFL, flags);

    int32_t val = 1;
    ::setsockopt(fd_listen, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if (ai->ai_family == AF_INET6) {
      ::setsockopt(fd_listen, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val));
    }

    if (::bind(fd_listen, ai->ai_addr, ai->ai_addrlen) < 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not bind socket (" << fd_listen << "), err="
          << strerror(errno) << std::endl;
      }
      ::shutdown(fd_listen, SHUT_RDWR);
      if (fd_listen != -1) {
        ::close(fd_listen);
      }
      continue;
    }

    if (listen(fd_listen, 100) < 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not listen to socket (" << fd_listen
          << "), err=" << strerror(errno) << std::endl;
      }
      ::shutdown(fd_listen, SHUT_RDWR);
      if (fd_listen != -1) {
        ::close(fd_listen);
      }
      continue;
    }

    {
      std::shared_ptr<EventData> event_data(new EventData(fd_listen));

      struct epoll_event event_listen;
      event_listen.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
      event_listen.data.ptr = event_data.get();
      epoll_ctl(fd_epoll, EPOLL_CTL_ADD, fd_listen, &event_listen);

      sockets[fd_listen] = event_data;
    }

    char host[64];
    getnameinfo(ai->ai_addr, ai->ai_addrlen, host, sizeof(host), nullptr, 0,
        NI_NUMERICHOST);

    if (m_verbosity > Verbosity::INFO) {
      std::clog << (ai->ai_family == AF_INET ? "IPv4" : "IPv6") << ": listen "
        << host << ":" << port << " (accepting " << fd_listen << ")"
        << std::endl;
    }

    has_listener = true;
  }
  freeaddrinfo(ais);

  if (!has_listener) {
    return -1;
  }

  std::atomic<bool> any_worker_ready(false);
  auto worker_job{[&ssl_ctx, &fd_epoll, &prepare_incoming_delegate,
    &append_incoming_delegate, &request_delegate, &sockets, &sockets_mutex,
    &client_count, &job_count, &disconnect, &close_session, &ssl_write,
    &do_support_sse, &any_worker_ready, this](uint32_t) -> int32_t
    {
      int32_t const max_events{100};
      uint32_t const event_timeout{1000};
      struct epoll_event events[max_events];

      struct epoll_event event_client;
      event_client.events = EPOLLIN | EPOLLET | EPOLLONESHOT;

      uint8_t buffer[TINYH2_BUFFER_SIZE];
      Http2Decoder decoder(buffer, TINYH2_BUFFER_SIZE);
      Http2Encoder encoder(buffer, TINYH2_BUFFER_SIZE);

      auto rearm_epoll{[&fd_epoll](struct epoll_event &event, int32_t fd) {
        event.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
        epoll_ctl(fd_epoll, EPOLL_CTL_MOD, fd, &event);
      }};

      any_worker_ready.store(true);

      class JobCounter {
        public:
          JobCounter(std::atomic<uint32_t> &job_count) noexcept:
            m_job_count(&job_count)
          {
            (*m_job_count)++;
          }
          JobCounter(JobCounter const &) = delete;
          JobCounter(JobCounter &&) = delete;
          ~JobCounter() noexcept
          {
            (*m_job_count)--;
          }

          JobCounter &operator=(JobCounter const &) = delete;
          JobCounter &operator=(JobCounter &&) = delete;

        private:
          std::atomic<uint32_t> *m_job_count;
      };

      do {
        int32_t nfds = epoll_wait(fd_epoll, events, max_events, event_timeout);
        if (nfds == -1) {
          if (errno == EINTR) {
            continue;
          }
          if (m_verbosity >= Verbosity::ON_ERROR) {
            std::cerr << "Error in epoll_wait, err=" << errno << std::endl;
          }
          return -1;
        }

        for (int32_t n = 0; n < nfds; ++n) {
          JobCounter job_counter(job_count);

          EventData *data = static_cast<EventData *>(events[n].data.ptr);
          std::lock_guard<std::mutex> const session_lock(data->session_mutex);

          bool is_local = false;
          if (data->fd_local != -1) {
            char buf[256];
            while (true) {
              int32_t rv = ::read(data->fd_local, buf, sizeof(buf));
              if (rv == 0) {
                break;
              } else if (rv < 0) {
                if (errno != EAGAIN) {
                  std::cerr << "Could not read from IPC (" << data->fd_remote
                    << "), err=" << strerror(errno) << std::endl;
                }
                break;
              }
              is_local = true;
              break;
            }
          }

          bool is_remote{false};
          if (!is_local) {
            char buf[8];
            int32_t rv = ::recv(data->fd_remote, buf, 8, MSG_PEEK);
            if (rv > 0) {
              is_remote = true;
            }
          }

          // TODO: This needs to be thought through. What are the different cases?
          // Draw a state machine. What happens if we have SSL, what if it's a listen
          // device? etc. And below, why do we rearm after disconnect??
          // In general. This whole thing needs a face lift. Separate EventData
          // for remote and local. Automatic cleanup of disconnected clients and streams
          // Ping from server? Btw. Ping from Chromium does not work?
          //if (data->socket_type != EventData::SocketType::Listen && !is_remote && !is_local) {
          //  disconnect(data->fd_remote, data->fd_local);
          //}
     //     The above made tests fail. But why was it there in the first place?

          if (data->socket_type == EventData::SocketType::Listen) {
            int32_t fd_remote;
            std::string client_addr;
            {
              struct sockaddr_in addr{};
              uint32_t addrlen = sizeof(addr);
              std::memset(&addr, 0, addrlen);
              fd_remote = ::accept4(data->fd_remote,
                  reinterpret_cast<struct sockaddr *>(&addr), &addrlen,
                  SOCK_NONBLOCK);

              client_addr = inet_ntoa(addr.sin_addr);
            }
            if (fd_remote == -1) {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Could not accept connection, err="
                  << strerror(errno) << std::endl;
                disconnect(data->fd_remote, data->fd_local);
                continue;
              }
              rearm_epoll(events[n], data->fd_remote);
              continue;
            }

            // TODO: Decide on TCP_NODELAY
            //    int32_t val = 1;
            //    ::setsockopt(fd_session, IPPROTO_TCP, TCP_NODELAY,
            //        reinterpret_cast<char *>(&val), sizeof(val));

            {
              struct timeval timeout_select{};
              fd_set fds{};
              timeout_select.tv_sec  = 0;
              timeout_select.tv_usec = 200000;
              FD_ZERO(&fds);
              FD_SET(fd_remote, &fds);
              int32_t r = ::select(fd_remote + 1, &fds, nullptr, nullptr,
                  &timeout_select);

              if (r > 0) {
                uint32_t const peek_len{128};
                std::string peek(peek_len, ' ');
                r = ::recv(fd_remote, peek.data(), peek_len, MSG_PEEK);
                if (r > 3 && peek.substr(0, 3) == "GET") {
                  ::recv(fd_remote, peek.data(), peek_len, 0);
                  std::string page(&peek[4],
                      &peek[peek.substr(4).find(" ") + 4]);

                  size_t const host_start = peek.find("Host:");
                  if (host_start != std::string::npos) {
                    std::string host(&peek[host_start + 6],
                        &peek[peek.substr(host_start).find("\n")
                        + host_start - 1]);

                    std::string response("HTTP/1.1 301 Moved Permanently\r\n"
                        "Server: Ola Benderius, tinyh2 v1\r\n"
                        "Location: https://" + host + page + "\r\n"
                        "Content-Length: 0\r\n\r\n");

                    if (m_verbosity > Verbosity::INFO) {
                      std::clog << "Requesting upgrade, http->https ("
                        << fd_remote << ")" << std::endl;
                    }

                    ::send(fd_remote, response.data(), response.length(), 0);
                  }
                  // TODO: Test case for this. Should not use sleep here.
                  //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                  int32_t nofd{-1};
                  disconnect(fd_remote, nofd);
                  rearm_epoll(events[n], data->fd_remote);
                  continue;
                }
              }
            }

            std::shared_ptr<SSL> ssl(SSL_new(&(*ssl_ctx)), [this](SSL *a_ssl) {
              if (m_verbosity >= Verbosity::INFO) {
                int32_t fd = SSL_get_fd(a_ssl);
                std::clog << "Deleting SSL session (" << fd << ")"
                  << std::endl;
              }
              SSL_set_shutdown(a_ssl,
                  SSL_get_shutdown(a_ssl) | SSL_RECEIVED_SHUTDOWN);
              SSL_shutdown(a_ssl);
              SSL_free(a_ssl);
            });

            if (!ssl) {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Could not initialize session (" << fd_remote
                  << "), err=" << ERR_error_string(ERR_get_error(), nullptr)
                  << std::endl;
              }
              int32_t nofd{-1};
              disconnect(fd_remote, nofd);
              rearm_epoll(events[n], data->fd_remote);
              continue;
            }

            int32_t rv = SSL_set_fd(ssl.get(), fd_remote);
            if (rv == 0) {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Could not start session (" << fd_remote
                  << "), err=" << ERR_error_string(ERR_get_error(), nullptr)
                  << std::endl;
              }
              int32_t nofd{-1};
              disconnect(fd_remote, nofd);
              rearm_epoll(events[n], data->fd_remote);
              continue;
            }

            SSL_set_accept_state(ssl.get());

            int32_t fd_local{-1};
            if (do_support_sse) {
              std::string const path_local = "p" + std::to_string(fd_remote)
                + ".ipc";

              ::mkfifo(path_local.c_str(), 0666);
              fd_local = ::open(path_local.c_str(), O_RDWR | O_NONBLOCK);

              int32_t flags = ::fcntl(fd_local, F_GETFL, 0) | O_NONBLOCK;
              ::fcntl(fd_local, F_SETFL, flags);
            }

            std::unique_ptr<Session> session(new Session(ssl, client_addr));
            std::shared_ptr<EventData> event_data(new EventData(fd_remote,
                  fd_local, std::move(session)));

            event_client.data.ptr = event_data.get();

            {
              std::lock_guard<std::mutex> const lock(sockets_mutex);
              sockets[fd_remote] = event_data;
              client_count++;
            }

            if (m_verbosity >= Verbosity::INFO) {
              std::clog << "New connection (hi " << fd_remote << ")"
                << std::endl;
              std::clog << "Clients connected: " << client_count.load() 
                << std::endl;
            }

            rv = epoll_ctl(fd_epoll, EPOLL_CTL_ADD, fd_remote, &event_client);
            if (rv == -1) {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Could not create remote event listener ("
                  << fd_remote << "), err=" << strerror(errno) << std::endl;
              }
              close_session(event_data.get());
              rearm_epoll(events[n], data->fd_remote);
              continue;
            }
            if (do_support_sse) {
              rv = epoll_ctl(fd_epoll, EPOLL_CTL_ADD, fd_local, &event_client);
              if (rv == -1) {
                if (m_verbosity >= Verbosity::ON_ERROR) {
                  std::cerr << "Could not create local event listener ("
                    << fd_remote << "), err=" << strerror(errno) << std::endl;
                }
                close_session(event_data.get());
                rearm_epoll(events[n], data->fd_remote);
                continue;
              }
            }

            rearm_epoll(events[n], data->fd_remote);

          } else if (data->session != nullptr) {
            Session *session = data->session.get();

            bool close_connection{false};

            bool send_ping{false};
            uint64_t opaque_data{0};

            bool send_settings_ack{false};
            std::map<uint32_t, uint32_t> window_updates;

            decoder.onData([this, &data, &session, &append_incoming_delegate](
                  uint32_t stream_id, bool is_end_stream, std::string d) {

                append_incoming_delegate(
                    session->streams[stream_id].data_in, d,
                    session->streams[stream_id].data_in_pos);
                session->streams[stream_id].data_in_pos += d.length();

                if (session->streams[stream_id].data_in->length() ==
                    session->streams[stream_id].data_in_pos) {
                  session->streams[stream_id].is_full_new_request = true;
                }

                if (is_end_stream) {
                  session->streams[stream_id].state
                    = Stream::State::HalfClosed;
                }

                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded DATA [type=0x0], stream="
                    << stream_id << ", is_end_stream=" << is_end_stream
                    << ", data=[" << d.length() << "] (" << data->fd_remote
                    << ")" << std::endl;
                }
              });
            decoder.onGoAway([this, &data, &close_connection](
                  uint32_t last_stream_id, uint32_t error_code,
                  std::string debug) {
                close_connection = true;
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded GOAWAY [type=0x7], last_stream_id="
                    << last_stream_id << ", error_code=" << error_code
                    << ", debug=[" << debug.length() << "] '" << debug << "' ("
                    << data->fd_remote << ")" << std::endl;
                }
              });
            decoder.onSettingsAck([this, &data]() {
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded SETTINGS [type=0x4], ACK ("
                    << data->fd_remote << ")" << std::endl;
                }
              });
            decoder.onSettings([this, &data, &session, &send_settings_ack](
                  std::map<uint8_t, uint32_t> settings) {
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded SETTINGS [type=0x4]";
                }
                for (auto& s : settings) {
                  // TODO: Make sure settings are honored
                  switch (s.first) {
                    case 1:
                      session->header_table_size = s.second;
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_HEADER_TABLE_SIZE="
                          << s.second;
                      }
                      break;
                    case 2:
                      if (s.second != 0) {
                        std::cout << "  .. TODO: IGNORING "
                          << "SETTINGS_ENABLE_PUSH: " << s.second << std::endl;
                      }
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_ENABLE_PUSH=" << s.second;
                      }
                      break;
                    case 3:
                      session->max_concurrent_streams = s.second;
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_MAX_CONCURRENT_STREAMS="
                          << s.second;
                      }
                      break;
                    case 4:
                      session->initial_stream_window_size = s.second;
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_INITIAL_WINDOW_SIZE="
                          << s.second;
                      }
                      break;
                    case 5:
                      session->max_frame_size = s.second;
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_MAX_FRAME_SIZE=" << s.second;
                      }
                      break;
                    case 6:
                      session->max_header_list_size = s.second;
                      if (m_verbosity >= Verbosity::DEBUG) {
                        std::clog << ", SETTINGS_MAX_HEADER_LIST_SIZE="
                          << s.second;
                      }
                      break;
                    default:
                      break;
                  };
                }
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << " (" << data->fd_remote << ")" << std::endl;
                }
                send_settings_ack = true;
              });
            decoder.onPing([this, &data, &send_ping, &opaque_data](
                  uint64_t a_opaque_data, bool is_ack) {
                opaque_data = a_opaque_data;
                send_ping = !is_ack;
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded PING [type=0x6], is_ack=" << is_ack
                    << " opaque_data=" << a_opaque_data << " ("
                    << data->fd_remote << ")" << std::endl;
                }
              });
            decoder.onPriority([this, &data](uint32_t stream_id, bool exclusive,
                  uint32_t dep_stream_id, uint8_t weight) {
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded PRIORITY [type=0x2], stream_id="
                    << stream_id << ", exclusive=" << exclusive
                    << ", dep_stream_id=" << dep_stream_id << ", weight="
                    << +weight << " (" << data->fd_remote << ")" << std::endl;
                }
              });
            decoder.onPreface([this, &data]() {
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded PREFACE (" << data->fd_remote << ")"
                    << std::endl;
                }
              });
            decoder.onRstStream([this, &data, &session](uint32_t stream_id,
                  uint32_t error_code) {
                session->streams[stream_id].state = Stream::State::Closed;
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded RST_STREAM [type=0x3], stream_id="
                    << stream_id << ", error_code=" << error_code
                    << " (" << data->fd_remote << ")" << std::endl;
                }
              });
            decoder.onHeader([this, &data, &session, &prepare_incoming_delegate,
                &window_updates](
                  uint32_t stream_id, bool is_end_stream, bool is_end_headers,
                  std::map<std::string, std::string> fields) {

                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded HEADER [type=0x1], stream_id="
                    << stream_id << ", is_end_stream=" << is_end_stream
                    << ", is_end_headers=" << is_end_headers << ", fields="
                    << fields.size() << " (" << data->fd_remote << ")"
                    << std::endl;
                    for (auto &f : fields) {
                      std::clog << "  " << f.first << " : " << f.second
                        << std::endl;
                    }
                }

                session->streams.emplace(
                    std::piecewise_construct,
                    std::forward_as_tuple(stream_id),
                    std::forward_as_tuple(session->initial_stream_window_size));
                session->streams[stream_id].fields = fields;

                if (fields.find("content-length") == fields.end()) {
                  session->streams[stream_id].is_full_new_request = true;
                } else {
                  uint32_t data_in_len;
                  try {
                    data_in_len = std::stoi(fields["content-length"]);

                    session->streams[stream_id].data_in =
                        prepare_incoming_delegate(data_in_len);

                    if (data_in_len > 0) {
                       window_updates[stream_id] = data_in_len;
                    }
                  } catch (...) {
                    if (m_verbosity >= Verbosity::ON_ERROR) {
                      std::cerr << "Client tried to send a non-string "
                        "content-length (" << data->fd_remote << "), str="
                        << fields["content-length"] << std::endl;
                    }
                  }
                }

                if (is_end_stream) {
                  session->streams[stream_id].state
                    = Stream::State::HalfClosed;
                }
              });
            decoder.onWindowUpdate([this, &data, &session](uint32_t stream_id,
                  uint32_t window_update) {
                if (m_verbosity >= Verbosity::DEBUG) {
                  std::clog << "Decoded WINDOW_UPDATE [type=0x8], stream_id="
                    << stream_id << ", window_update=" << window_update
                    << " (" << data->fd_remote << ")" << std::endl;
                }
                if (stream_id == 0) {
                  session->window_size += window_update;
                } else {
                  session->streams[stream_id].window_size_out += window_update;
                }
              });

            if (is_remote && !session->is_handshaked) {
              ERR_clear_error();
              int32_t rv = SSL_do_handshake(session->ssl.get());
              if (rv > 0) {
                unsigned char const *proto = nullptr;
                uint32_t proto_len;
                SSL_get0_alpn_selected(session->ssl.get(), &proto, &proto_len);
                std::string p(reinterpret_cast<char const *>(proto), proto_len);

                if (SSL_session_reused(session->ssl.get())
                    && m_verbosity >= Verbosity::INFO) {
                  std::clog << "[reused] ";
                }
                if (m_verbosity >= Verbosity::INFO) {
                  std::clog << "HTTPS link established, proto='" << p << "' ("
                    << data->fd_remote << ")" << std::endl;
                }

                session->is_handshaked = true;
                encoder.encodeSettingsDefaultServer();
              } else if (rv < 0) {
                auto e = SSL_get_error(session->ssl.get(), rv);
                if (e != SSL_ERROR_WANT_READ && e != SSL_ERROR_WANT_WRITE) {
                  if (m_verbosity >= Verbosity::INFO && e == SSL_ERROR_SYSCALL
                      && errno == 0) {
                    std::cerr << "Client left before handshake ("
                      << data->fd_remote << ")" << std::endl;
                  } else if (m_verbosity >= Verbosity::ON_ERROR) {
                    std::cerr << "Handshake failed (" << data->fd_remote
                      << ") err=" << e << std::endl;
                  }
                  close_session(data);
                }
              } else {
                // TODO: Should there be a rearm here? No, test cases fail then
                // due to trigger loop. Why?
                continue;
              }
            } else {
              if (is_remote) {
                {
                  if (data->session == nullptr) {
                    continue;
                  }

                  int32_t rv = SSL_read(session->ssl.get(), decoder.data(),
                      decoder.maxLength());
                  if (rv > 0) {
                    if (m_verbosity >= Verbosity::DEBUG) {
                      std::clog << "Data received, length=" << rv
                        << " (" << data->fd_remote << ")" << std::endl;
                      uint32_t const max_print_len{1600};
                      printHex(decoder.data(),
                          std::min(static_cast<uint32_t>(rv), max_print_len));
                      if (rv > static_cast<int64_t>(max_print_len)) {
                        std::clog << "  " << (rv - max_print_len)
                          << " bytes omitted in printout." << std::endl;
                      }
                    }

                    auto r = decoder.decode(session->dynamic_table,
                        session->max_header_list_size, rv);
                    if (r.first == Http2Decoder::StatusCode::UnknownType
                        && m_verbosity >= Verbosity::ON_ERROR) {
                      std::cerr << "Unknown frame type (" << data->fd_remote
                        << ", " << session->client_addr << "), type="
                        << r.second << std::endl;
                    } else if (r.first == Http2Decoder::StatusCode::DecodeError
                        && m_verbosity >= Verbosity::ON_ERROR) {
                      std::cerr << "Decode error (" << data->fd_remote
                        << ", " << session->client_addr << "), err="
                        << r.second << std::endl;
                    } else if (r.first == Http2Decoder::StatusCode::FrameError
                        && m_verbosity >= Verbosity::ON_ERROR) {
                      std::cerr << "Empty or corrupt frame ("
                        << data->fd_remote << ", " << session->client_addr
                        << ")" << std::endl;
                    }
                  } else if (rv < 0) {
                    close_connection = true;
                  } else {
                    // TODO: Should there be a rearm here? No, test cases fail
                    // then due to trigger loop. Why?
                    continue;
                  }
                }

                decoder.reset();

                if (send_ping) {
                  send_ping = false;
                  encoder.encodePing(opaque_data, true);
                  opaque_data = 0;
                }

                if (send_settings_ack) {
                  send_settings_ack = false;
                  encoder.encodeSettingsAck();
                }

                for (auto &wu : window_updates) {
                  encoder.encodeWindowUpdate(wu.second, wu.first);
                  encoder.encodeWindowUpdate(wu.second, 0);
                }
              }

              for (auto &stream : session->streams) {
                std::lock_guard<std::mutex> const stream_lock(
                    stream.second.stream_mutex);

                uint32_t stream_id = stream.first;
                if (stream.second.is_full_new_request) {
                  stream.second.is_full_new_request = false;

                  // TODO: This can be called multiple times from the client
                  // Split to set_header(), and add_data(). Then send all.
                  auto send_response{[&data, stream_id, &encoder,
                    &ssl_write, &close_connection](
                      std::string content_type, uint32_t status_code,
                      std::shared_ptr<std::string const> content) {

                    if (content == nullptr) {
                      content = std::shared_ptr<std::string>(new std::string);
                    }

                    bool is_stream_end = (content->size() == 0);

                    data->session->streams[stream_id].data_out = content;
                    encoder.encodeHeader(content_type, status_code,
                        content->length(), stream_id, is_stream_end);

                    close_connection = ssl_write(data, encoder);
                  }};

                  auto register_for_event_source{[this, &session, &ssl_write,
                    stream_id, &data, &encoder, &close_connection](
                        std::string event_source, void *client_data,
                        std::string const &greetings) {

                    if (m_verbosity >= Verbosity::INFO) {
                      if (m_event_sources.find(event_source) ==
                          m_event_sources.end()) {
                        std::clog << "SSE: Adding new event source '"
                          << event_source << std::endl;
                      }
                      if (m_event_sources[event_source][data].find(stream_id)
                          != m_event_sources[event_source][data].end()) {
                        std::clog << "SSE: Event source '" << event_source
                          << "': Updating client data ("
                          << data->fd_remote << ":" << stream_id << ")"
                          << std::endl;
                      } else {
                        std::clog << "SSE: Event source '" << event_source
                          << "': Adding client (" << data->fd_remote << ":"
                          << stream_id << ")" << std::endl;
                      }
                    }

                    m_event_sources[event_source][data][stream_id] =
                      client_data;


                    for (auto &es : m_event_sources) {
                      std::cout << " .. es " << es.first << std::endl;
                      for (auto &c : es.second) {
                        std::cout << " .... " << c.first->fd_remote
                          << std::endl;
                        for (auto &s : c.second) {
                          std::cout << " ...... " << s.first << std::endl;
                        }
                      }
                    }
                    std::cout << " --- " << std::endl;

                    std::string msg = greetings;
                    if (msg.empty()) {
                      msg = ":\r\n\r\n";
                    }

                    std::shared_ptr<std::string> content(new std::string(msg));

                    session->streams[stream_id].is_server_side_event = true;
                    session->streams[stream_id].data_out = content;
                    encoder.encodeHeader("text/event-stream", 200,
                        0, stream_id, false);

                    close_connection = ssl_write(data, encoder);
                  }};

#ifdef TINYH2_DEBUG
                  if (stream.second.fields[":path"] == "/tinyh2debug/info") {
                    uint32_t status_code = 200;
                    std::string content_type = "text/plain";

                    std::shared_ptr<std::string const> content_p;
                    {
                      std::lock_guard<std::mutex> const lock(m_debug_mutex);
                      content_p.reset(new std::string(m_debug_socketinfo));
                    }
                    send_response(content_type, status_code, content_p);
                  } else {
#endif
                    request_delegate(stream.second.fields,
                        stream.second.data_in, session->client_addr,
                        send_response, register_for_event_source);
#ifdef TINYH2_DEBUG
                  }
#endif
                }

                auto data_out = session->streams[stream_id].data_out;
                uint32_t bytes_left{0};
                if (data_out != nullptr) {
                  bytes_left = data_out->length()
                    - session->streams[stream_id].data_out_pos;
                }

                while (session->window_size > 0
                    && session->streams[stream_id].window_size_out > 0
                    && bytes_left > 0
                    && encoder.space() - 9 > 0) {

                  int32_t content_length =
                    std::min(bytes_left,
                      std::min(encoder.space() - 9,
                        std::min(session->max_frame_size,
                          std::min(
                            static_cast<uint32_t>(session->window_size),
                            static_cast<uint32_t>(
                              session->streams[stream_id].window_size_out)
                            ))));

                  void const *d = reinterpret_cast<void const *>(
                      data_out->data()
                      + session->streams[stream_id].data_out_pos);
                  bool is_last_fragment =
                    (static_cast<int64_t>(bytes_left) == content_length);
                  bool is_end_of_stream = is_last_fragment
                    && !session->streams[stream_id].is_server_side_event;

                  encoder.encodeData(d, content_length, stream_id,
                      is_end_of_stream);

                  if (is_end_of_stream) {
                    session->streams[stream_id].state = Stream::State::Closed;
                  }

                  session->window_size -= content_length;
                  session->streams[stream_id].window_size_out -= content_length;

                  bytes_left -= content_length;
                  session->streams[stream_id].data_out_pos += content_length;

                  close_connection = ssl_write(data, encoder);
                }
                if (bytes_left == 0) {
                  session->streams[stream_id].data_out = nullptr;
                  session->streams[stream_id].data_out_pos = 0;
                }
              }
            }
            close_connection = ssl_write(data, encoder);
            if (close_connection) {
              close_session(data);
            }
            if (is_local) {
              rearm_epoll(events[n], data->fd_local);
            }
            if (is_remote) {
              rearm_epoll(events[n], data->fd_remote);
            }
          }
        }
      } while (m_is_running.load());
      return 0;
    }};

  std::vector<std::thread> workers;
  for (uint32_t i = 0; i < worker_count; ++i) {
    workers.push_back(std::thread(worker_job, i));
  }

  while (!any_worker_ready.load()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  m_is_running.store(true);
  {
    uint8_t buffer[TINYH2_BUFFER_SIZE];
    Http2Encoder encoder(buffer, TINYH2_BUFFER_SIZE);
    while ((m_is_running.load() && !got_signal.load())
        || job_count.load() > 0) {
      std::vector<int32_t> doomed_sockets;
      {
        std::lock_guard<std::mutex> const lock(sockets_mutex);
      
        for (auto &socket : sockets) {
          if (socket.second != nullptr
              && socket.second->socket_type == EventData::SocketType::Client
              && socket.second->session != nullptr) {

            std::lock_guard<std::mutex> const session_lock(
                socket.second->session_mutex);

            if (socket.second->session->is_handshaked) {

              std::vector<uint32_t> doomed_streams;
              for (auto &s : socket.second->session->streams) {
                if (s.second.state == Stream::State::Closed) {
                  doomed_streams.push_back(s.first);
                }
              }
              for (auto &id : doomed_streams) {
                socket.second->session->streams.erase(id);
              }

              uint64_t opaque_data = 0;
              encoder.encodePing(opaque_data, false);

              bool close_connection = ssl_write(socket.second.get(), encoder);
              if (close_connection) {
                doomed_sockets.push_back(socket.first);
              }
            }
          }
        }
      }

#ifdef TINYH2_DEBUG
      {
        std::stringstream ss;
        ss << "--- TINYH2 DEBUG INFO ---" << std::endl;

        {
          std::lock_guard<std::mutex> const lock0(sockets_mutex);
          for (auto sp : sockets) {
            auto s = sp.second;
            ss << "key " << sp.first << ":" << std::endl
              << "  type: " << (s->socket_type == EventData::SocketType::Listen
                  ? "listen" : "client") << std::endl
              << "  fd_remote: " << s->fd_remote << std::endl
              << "  fd_local: " << s->fd_local << std::endl
              << "  session: " << (s->session == nullptr ? "null" : "")
              << std::endl;
            if (s->session != nullptr) {
              ss << "    header_table_size: " << s->session->header_table_size
                << std::endl
                << "    initial_stream_window_size: "
                << s->session->initial_stream_window_size << std::endl
                << "    max_concurrecnt_streams: "
                << s->session->max_concurrent_streams << std::endl
                << "    max_header_list_size: "
                << s->session->max_header_list_size << std::endl
                << "    max_frame_size: " << s->session->max_frame_size
                << std::endl
                << "    window_size: " << s->session->window_size << std::endl
                << "    is_handshaked: " << (s->session->is_handshaked
                    ? "yes" : "no") << std::endl
                << "    ssl: " << (s->session->ssl == nullptr
                    ? "null" : "active") << std::endl
                << "    dynamic table:" << std::endl;
              for (auto dtp : s->session->dynamic_table) {
                ss << "      " << dtp.first << " : " << dtp.second << std::endl;
              }
              ss << "    streams:" << std::endl;
              for (auto &stp : s->session->streams) {
                auto &st = stp.second;
                std::lock_guard<std::mutex> const lock2(st.stream_mutex);
                ss << "      id: " << stp.first << std::endl
                  << "        state: " << (st.state == Stream::State::Open
                      ? "open" : st.state == Stream::State::Closed
                      ? "closed" : "half closed") << std::endl
                  << "        data_in_pos: " << st.data_in_pos << std::endl
                  << "        data_out_pos: " << st.data_out_pos << std::endl
                  << "        window_size_in: " << st.window_size_in
                  << std::endl
                  << "        window_size_out: " << st.window_size_out
                  << std::endl
                  << "        is_full_new_request: " << (st.is_full_new_request
                      ? "yes" : "no") << std::endl
                  << "        is_server_side_event: "
                  << (st.is_server_side_event ? "yes" : "no") << std::endl
                  << "        data_in: " << (st.data_in == nullptr
                      ? "null" : std::to_string(st.data_in->size()) + " bytes")
                  << std::endl
                  << "        data_out: " << (st.data_out == nullptr
                      ? "null" : std::to_string(st.data_out->size()) + " bytes")
                  << std::endl
                  << "        fields:" << std::endl;
                for (auto fp : st.fields) {
                  ss << "          " << fp.first << " : " << fp.second
                    << std::endl;
                }
              }
            }
          }
        }
        std::lock_guard<std::mutex> const lock(m_debug_mutex);
        m_debug_socketinfo = ss.str();
      }
#endif

      // TODO: Try this. Close first, then remove.
      // Lock again to make sure no clients are waiting.
      // Since connection is closed, no more clients should be able to join.
      // A previous bug was that the socket was "erased" when a session was
      // worked on (from epoll).
      for (auto &id : doomed_sockets) {
        {
          std::lock_guard<std::mutex> const session_lock(
              sockets[id]->session_mutex);
          close_session(sockets[id].get());
        }
        std::lock_guard<std::mutex> const session_lock(
            sockets[id]->session_mutex);
      }
      for (auto &id : doomed_sockets) {
        std::lock_guard<std::mutex> const lock(sockets_mutex);
        client_count -= sockets.erase(id);
      }

      if (m_verbosity >= Verbosity::DEBUG) {
        std::clog << "Clients connected: " << client_count.load() << std::endl;
        std::clog << "Ongoing jobs: " << job_count.load() << std::endl;
      }

      // TODO: Ping frequency as argument.
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    m_is_running.store(false);
  }

  for (auto &worker : workers) {
    worker.join();
  }

  for (auto fd_listen : fds_listen) {
    ::shutdown(fd_listen, SHUT_RDWR);
    if (fd_listen != -1) {
      ::close(fd_listen);
    }
  }

  {
    std::vector<int32_t> doomed_sockets;
    for (auto &socket : sockets) {
      if (socket.second != nullptr
          && socket.second->socket_type == EventData::SocketType::Client) {
        doomed_sockets.push_back(socket.first);
      }
    }
    for (auto &id : doomed_sockets) {
      close_session(sockets[id].get());
    }
  }

  return 0;
}

// TODO: Use allocator from outside.. Should not use "new" in here!
inline void Server::sendEvents(std::string event_source,
    std::function<std::string(void *)> event_composer)
      noexcept
{
  // TODO: Clean up lists to remove zombies.
  for (auto c : m_event_sources[event_source]) {
    EventData *data = c.first;
    if (data == nullptr) {
    //  std::cout << "SKIPPING DATA" << std::endl;
      continue;
    }
    for (auto s : c.second) {
      Session *session = data->session.get();
      uint32_t stream_id = s.first;

      if (session == nullptr ||
          session->streams[stream_id].state == Stream::State::Closed) {
     //   std::cout << "SKIPPING STREAM" << std::endl;
        continue;
      }

      std::string content = event_composer(s.second);

      //std::cout << "SENDING EVENT " << content << std::endl;

      if (!content.empty()) {
        std::lock_guard<std::mutex> const stream_lock(
            session->streams[stream_id].stream_mutex);
        session->streams[stream_id].data_out =
            std::shared_ptr<std::string const>(new std::string(content));
      }
    }

    std::string const path_local = "p" + std::to_string(data->fd_remote)
      + ".ipc";
    std::string const wake_msg = "go_" + std::to_string(data->fd_remote);

    size_t r = ::write(data->fd_local, wake_msg.c_str(), wake_msg.length());
    if (r <= 0) {
      std::cerr << "Could not write to IPC (" << data->fd_remote << "), err="
        << strerror(errno) << std::endl;
    }
  }
}

inline void Server::stop() noexcept
{
  m_is_running.store(false);
}

class Client {
  public:
    Client(std::string const &, uint32_t, Verbosity) noexcept;
    Client(Client const &) = delete;
    Client(Client &&) = delete;
    ~Client() noexcept;

    Client &operator=(Client const &) = delete;
    Client &operator=(Client &&) = delete;

 /*   void get(std::string const &, std::function<void(std::string const &)>)
      noexcept;
    void post(std::string const &, std::string const &,
        std::function<void(std::string const &)>, std::string const &) noexcept;
*/
  private:
    SSL *m_ssl;
    uint8_t *m_buffer_decoder;
    uint8_t *m_buffer_encoder;
    uint32_t m_verbosity;
};

inline Client::Client(std::string const &host, uint32_t port,
    Verbosity verbosity) noexcept:
  m_ssl(SSL_new(SSL_CTX_new(TLS_client_method()))),
  m_buffer_decoder(),
  m_buffer_encoder(),
  m_verbosity(verbosity)
{
  // TODO: Only when verbosity is high.
  SSL_load_error_strings();
  ERR_load_crypto_strings();

  ERR_clear_error();

  if (!m_ssl) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Could not initialize session, err="
        << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
    }
    return;
  }

  int32_t fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd < 0) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Could not create socket, err="
        << ::strerror(errno) << std::endl;
    }
    return;
  }
  {
    struct sockaddr_in addr{};
    std::memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ::inet_addr(host.c_str());
    addr.sin_port = htons(port);

    int32_t r = ::connect(fd, reinterpret_cast<struct sockaddr *>(&addr),
        sizeof(addr));
    if (r < 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not connect to server, err="
          << ::strerror(errno) << std::endl;
      }
      return;
    }
  }

  SSL_library_init();
  SSLeay_add_ssl_algorithms();

  {
    int32_t r = SSL_set_fd(m_ssl, fd);
    if (r == 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not start session, err="
          << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
      }
      return;
    }
  }
  {
    int32_t r = SSL_connect(m_ssl);
    if (r <= 0) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Could not connect to SSL session, err="
          << ERR_error_string(ERR_get_error(), nullptr) << std::endl;
      }
      return;
    }
  }
  if (m_verbosity >= Verbosity::INFO) {
    std::clog << "Connected, using " << SSL_get_cipher(m_ssl) << std::endl;
  }

  m_buffer_decoder = reinterpret_cast<uint8_t *>(::malloc(TINYH2_BUFFER_SIZE));
  m_buffer_encoder = reinterpret_cast<uint8_t *>(::malloc(TINYH2_BUFFER_SIZE));

  Http2Encoder decoder(m_buffer_decoder, TINYH2_BUFFER_SIZE);
  Http2Encoder encoder(m_buffer_encoder, TINYH2_BUFFER_SIZE);

  {
    encoder.encodePreface();
    SSL_write(m_ssl, encoder.data(), encoder.length());
    encoder.reset();
  }
  {
    encoder.encodeSettingsDefaultClient();
    SSL_write(m_ssl, encoder.data(), encoder.length());
    encoder.reset();
  }
}

inline Client::~Client()
{
  SSL_free(m_ssl);
  free(m_buffer_decoder);
  free(m_buffer_encoder);
}

/*
inline void Client::get(std::string const &url,
    std::function<void(std::string const &)> callback) noexcept
{
}

inline void Client::post(std::string const &url,
    std::string const &data, std::function<void(std::string const &)> callback,
    std::string const &contentType = "application/json") noexcept
{
}
*/

class SimpleReverseProxyServer {
  public:
    SimpleReverseProxyServer(Verbosity = Verbosity::ON_ERROR) noexcept;
    SimpleReverseProxyServer(SimpleReverseProxyServer const &) = delete;
    SimpleReverseProxyServer(Server &&) = delete;
    ~SimpleReverseProxyServer() = default;

    SimpleReverseProxyServer &operator=(
        SimpleReverseProxyServer const &) = delete;
    SimpleReverseProxyServer &operator=(SimpleReverseProxyServer &&) = delete;

    bool isRunning() const;
    int32_t run(std::string, std::string, uint32_t, uint32_t, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<std::tuple<std::string, uint32_t,
        std::shared_ptr<std::string const>>(std::map<std::string,
          std::string>, std::shared_ptr<std::string>, std::vector<std::string>,
          std::function<std::tuple<std::string, uint32_t,
          std::shared_ptr<std::string const>>(std::map<std::string,
            std::string>, std::shared_ptr<std::string>,
            std::string const &)>)>) noexcept;
    int32_t run(std::string, std::string, std::string, uint32_t, uint32_t,
        uint32_t, std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<std::tuple<std::string, uint32_t,
        std::shared_ptr<std::string const>>(std::map<std::string,
          std::string>, std::shared_ptr<std::string>, std::vector<std::string>,
          std::function<std::tuple<std::string, uint32_t,
          std::shared_ptr<std::string const>>(std::map<std::string,
            std::string>, std::shared_ptr<std::string>,
            std::string const &)>)>) noexcept;
    int32_t run(std::string, std::string, uint32_t, uint32_t, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<std::tuple<std::string, uint32_t,
        std::shared_ptr<std::string const>>(std::map<std::string,
          std::string>, std::shared_ptr<std::string>, std::string const &,
          std::vector<std::string>,
          std::function<std::tuple<std::string, uint32_t,
          std::shared_ptr<std::string const>>(std::map<std::string,
            std::string>, std::shared_ptr<std::string>,
            std::string const &)>)>) noexcept;
    int32_t run(std::string, std::string, std::string, uint32_t, uint32_t,
        uint32_t, std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::shared_ptr<std::string>, std::string const &,
          uint32_t)>,
        std::function<std::tuple<std::string, uint32_t,
        std::shared_ptr<std::string const>>(std::map<std::string,
          std::string>, std::shared_ptr<std::string>, std::string const &,
          std::vector<std::string>,
          std::function<std::tuple<std::string, uint32_t,
          std::shared_ptr<std::string const>>(std::map<std::string,
            std::string>, std::shared_ptr<std::string>,
            std::string const &)>)>) noexcept;
    void stop() noexcept;

  private:
    Server m_server;
    Verbosity m_verbosity;
    int32_t m_fd_listen;
};

inline SimpleReverseProxyServer::SimpleReverseProxyServer(Verbosity verbosity)
  noexcept:
  m_server(verbosity),
  m_verbosity(verbosity),
  m_fd_listen()
{
}

inline bool SimpleReverseProxyServer::isRunning() const
{
  return m_server.isRunning();
}

inline int32_t SimpleReverseProxyServer::run(
    std::string private_key_path, std::string public_key_path,
    uint32_t port_external, uint32_t port_internal, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<std::tuple<std::string, uint32_t,
    std::shared_ptr<std::string const>>(std::map<std::string,
      std::string>, std::shared_ptr<std::string>, std::vector<std::string>,
      std::function<std::tuple<std::string, uint32_t,
      std::shared_ptr<std::string const>>(std::map<std::string,
        std::string>, std::shared_ptr<std::string>,
        std::string const &)>)> local_request_delegate)
    noexcept
{
  return run(private_key_path, public_key_path, "", port_external,
      port_internal, worker_count, prepare_incoming_delegate,
      append_incoming_delegate, local_request_delegate);
}

inline int32_t SimpleReverseProxyServer::run(
    std::string private_key_path, std::string public_key_path,
    std::string address_external, uint32_t port_external,
    uint32_t port_internal, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<std::tuple<std::string, uint32_t,
    std::shared_ptr<std::string const>>(std::map<std::string,
      std::string>, std::shared_ptr<std::string>, std::vector<std::string>,
      std::function<std::tuple<std::string, uint32_t,
      std::shared_ptr<std::string const>>(std::map<std::string,
        std::string>, std::shared_ptr<std::string>,
        std::string const &)>)> local_request_delegate)
    noexcept
{
  auto local_request_delegate_2{
    [&local_request_delegate](std::map<std::string, std::string> fields,
        std::shared_ptr<std::string> data, std::string const &,
        std::vector<std::string> filters,
        std::function<std::tuple<std::string, uint32_t,
        std::shared_ptr<std::string const>>(
          std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::string const &)> request_message
        ) -> std::tuple<std::string, uint32_t,
    std::shared_ptr<std::string const>> {
      return local_request_delegate(fields, data, filters, request_message);
    }};
  return run(private_key_path, public_key_path, address_external,
      port_external, port_internal, worker_count, prepare_incoming_delegate,
      append_incoming_delegate, local_request_delegate_2);
}

inline int32_t SimpleReverseProxyServer::run(
    std::string private_key_path, std::string public_key_path,
    uint32_t port_external, uint32_t port_internal, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<std::tuple<std::string, uint32_t,
    std::shared_ptr<std::string const>>(std::map<std::string,
      std::string>, std::shared_ptr<std::string>, std::string const &,
      std::vector<std::string>,
      std::function<std::tuple<std::string, uint32_t,
      std::shared_ptr<std::string const>>(std::map<std::string,
        std::string>, std::shared_ptr<std::string>,
        std::string const &)>)> local_request_delegate)
    noexcept
{
  return run(private_key_path, public_key_path, "", port_external,
      port_internal, worker_count, prepare_incoming_delegate,
      append_incoming_delegate, local_request_delegate);
}
  
inline int32_t SimpleReverseProxyServer::run(
    std::string private_key_path, std::string public_key_path,
    std::string address_external, uint32_t port_external,
    uint32_t port_internal, uint32_t worker_count,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate, std::function<void(std::shared_ptr<std::string>,
      std::string const &, uint32_t)> append_incoming_delegate,
    std::function<std::tuple<std::string, uint32_t,
    std::shared_ptr<std::string const>>(std::map<std::string,
      std::string>, std::shared_ptr<std::string>, std::string const &,
      std::vector<std::string>,
      std::function<std::tuple<std::string, uint32_t,
      std::shared_ptr<std::string const>>(std::map<std::string,
        std::string>, std::shared_ptr<std::string>,
        std::string const &)>)> local_request_delegate)
    noexcept
{
  struct BackendClient {
    std::string filter;
    std::mutex mutex;
    uint32_t request_count;
    int32_t fd;

    BackendClient():
      filter(),
      mutex(),
      request_count(),
      fd() {}

    BackendClient(std::string a_filter, int32_t a_fd):
      filter(a_filter),
      mutex(),
      request_count(),
      fd(a_fd) {}
  };

  sigset_t set;
  ::sigemptyset(&set);
  ::sigaddset(&set, SIGPIPE);
  ::pthread_sigmask(SIG_BLOCK, &set, nullptr);

  m_fd_listen = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (m_fd_listen == -1) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Reverse proxy server, could not create socket" << std::endl;
    }
    return -1;
  }

  int32_t val = 1;
  ::setsockopt(m_fd_listen, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));

  struct sockaddr_in address;
  ::memset(&address, 0, sizeof(address));
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = htonl(INADDR_ANY);
  address.sin_port = htons(port_internal);

  if (::bind(m_fd_listen, reinterpret_cast<struct sockaddr *>(&address),
      sizeof(address)) < 0) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Reverse proxy server, could not bind socket ("
        << m_fd_listen << "), err=" << strerror(errno) << std::endl;
    }
    ::shutdown(m_fd_listen, SHUT_RDWR);
    if (m_fd_listen != -1) {
      ::close(m_fd_listen);
    }
    return -1;
  }

  uint32_t const max_connections{100};
  if (::listen(m_fd_listen, max_connections) < 0) {
    if (m_verbosity >= Verbosity::ON_ERROR) {
      std::cerr << "Reverse proxy server, could not listen to socket ("
        << m_fd_listen << "), err=" << strerror(errno) << std::endl;
    }
    ::shutdown(m_fd_listen, SHUT_RDWR);
    if (m_fd_listen != -1) {
      ::close(m_fd_listen);
    }
    return -1;
  }

  std::mutex clients_mutex;
  std::map<std::string, BackendClient> clients;

  std::mutex global_req_count_mutex;
  uint32_t global_req_count{0};

  std::atomic<bool> is_running(true);

  auto close_session{[this, &clients](int32_t &fd) {
    std::string filter_to_remove;
    for (auto const &c : clients) {
      if (c.second.fd == fd) {
        filter_to_remove = c.second.filter;
        break;
      }
    }
    int32_t fd_copy = fd;
    ::shutdown(fd, SHUT_RDWR);
    if (fd != -1) {
      ::close(fd);
    }
    fd = -1;
    if (!filter_to_remove.empty()) {
      clients.erase(filter_to_remove);
      if (m_verbosity >= Verbosity::INFO) {
        std::clog << "Reverse proxy server, client filter removed (" << fd_copy
          << "): '" << filter_to_remove << "'" << std::endl;
      }
    }
    if (m_verbosity >= Verbosity::INFO) {
      std::clog << "Reverse proxy server, connection closed (" << fd_copy << ")"
        << std::endl;
    }
  }};

  auto worker_job{[&is_running, &clients_mutex, &clients, &global_req_count,
    &global_req_count_mutex, &close_session, &prepare_incoming_delegate,
    this]() -> int32_t {

      uint32_t const buffer_size{512};
      uint8_t buffer[buffer_size];

      struct timeval timeout_select{};
      struct timeval timeout_recv{};
      fd_set fds{};

      while (is_running.load()) {
        timeout_select.tv_sec  = 1;
        timeout_select.tv_usec = 0;
        FD_ZERO(&fds);
        FD_SET(m_fd_listen, &fds);
        int32_t rv = ::select(m_fd_listen + 1, &fds, nullptr, nullptr,
            &timeout_select);
        if (rv == 0) {
          continue;
        }

        int32_t fd_client;
        std::string client_addr;
        {
          struct sockaddr_in addr{};
          uint32_t addrlen = sizeof(addr);
          std::memset(&addr, 0, addrlen);
          fd_client = ::accept4(m_fd_listen,
              reinterpret_cast<struct sockaddr *>(&addr), &addrlen, 0);
          client_addr = inet_ntoa(addr.sin_addr);
        }
        if (fd_client == -1) {
          if (errno != 22 && m_verbosity >= Verbosity::ON_ERROR) {
            std::cerr << "Reverse proxy server, could not accept connection ("
              << m_fd_listen << ", " << client_addr << "), err=" << errno
              << std::endl;
          }
          break;
        }

        if (client_addr != "127.0.0.1") {
          if (m_verbosity >= Verbosity::INFO) {
            std::cerr << "Reverse proxy server, will not accept connection ("
              << fd_client << ", " << client_addr << ")" << std::endl;
          }
          ::shutdown(fd_client, SHUT_RDWR);
          if (fd_client != -1) {
            ::close(fd_client);
          }
          continue;
        }

        timeout_recv.tv_sec = 1;
        timeout_recv.tv_usec = 0;
        ::setsockopt(fd_client, SOL_SOCKET, SO_RCVTIMEO, &timeout_recv,
            sizeof(timeout_recv));

        if (m_verbosity >= Verbosity::INFO) {
          std::clog << "Reverse proxy server, accepted connection ("
            << fd_client << ", " << client_addr << ")" << std::endl;
        }

        uint32_t d_n;

        bool is_peeking{true};
        uint32_t bytes_to_read{0};
        std::string incoming;
        while (is_peeking || incoming.length() < bytes_to_read) {
          int32_t r;
          {
            std::lock_guard<std::mutex> const lock(clients_mutex);
            if (is_peeking) {
              r = ::recv(fd_client, buffer, 512, MSG_PEEK);
            } else {
              uint32_t n = std::min(
                  static_cast<int32_t>(bytes_to_read - incoming.length()), 512);
              r = ::recv(fd_client, buffer, n, 0);
            }
          }

          if (r == 0) {
            if (m_verbosity >= Verbosity::INFO) {
              std::clog << "Reverse proxy server, connection closed ("
                << fd_client << ", " << client_addr << ")" << std::endl;
            }
            close_session(fd_client);
            break;
          } else if (r < 0) {
            if (errno == 11) {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Reverse proxy server, no filter given ("
                  << fd_client << ", " << client_addr << ")" << std::endl;
              }
            } else {
              if (m_verbosity >= Verbosity::ON_ERROR) {
                std::cerr << "Reverse proxy server, connection error ("
                  << fd_client << ", " << client_addr << "), err="
                  << ::strerror(errno) << std::endl;
              }
            }
            close_session(fd_client);
            break;
          } else if (is_peeking && r >= 5) {
            ::memcpy(&d_n, buffer + 1, 4);
            bytes_to_read = ntohl(d_n);
            is_peeking = false;
            continue;
          }
          if (!is_peeking) {
            std::string str(r, ' ');
            ::memcpy(str.data(), buffer, r);
            incoming += str;
          }
        }

        if (incoming[0] != 0) {
          if (m_verbosity >= Verbosity::ON_ERROR) {
            std::cerr << "Reverse proxy server, unexpected filter ("
              << fd_client << ", " << client_addr << ")" << std::endl;
          }
          close_session(fd_client);
          continue;
        }

        uint32_t filter_length = incoming.length() - 5;
        std::string filter(filter_length, ' ');
        ::memcpy(filter.data(), incoming.data() + 5, filter_length);

        if (m_verbosity >= Verbosity::INFO) {
          std::clog << "Reverse proxy server, client filter added ("
            << fd_client << ", " << client_addr << "): '" <<  filter << "'"
            << std::endl;
        }

        std::lock_guard<std::mutex> const lock(clients_mutex);
        if (clients.find(filter) != clients.end()) {
          close_session(clients[filter].fd);
        }
        clients.emplace(std::piecewise_construct,
            std::forward_as_tuple(filter),
            std::forward_as_tuple(filter, fd_client));
      }

      m_server.stop();
      return 0;
    }};

  std::thread worker(worker_job);

  auto request_message{
    [this, &clients_mutex, &clients, &global_req_count_mutex, &global_req_count,
      &close_session](std::map<std::string, std::string> fields,
        std::shared_ptr<std::string> data_in,
        std::string const &subject_data) -> std::tuple<std::string,
        uint32_t, std::shared_ptr<std::string const>>
      {
        uint32_t status_code = 404;
        std::string content_type = "text/html; charset=UTF-8";
        std::string content = "<html><head><title>404: Not Found"
          "</title></head><body><h1>404: Not Found</h1><hr><address>"
          "tinyh2, The header-only HTTP/2 server</address></body>"
          "</html>";

        std::string match;
        {
          uint32_t best_flen{0};
          std::lock_guard<std::mutex> const lock(clients_mutex);
          for (auto &c : clients) {
            std::string f = c.second.filter;
            uint32_t flen = f.length();
            if (fields[":path"].substr(0, flen) == f && flen > best_flen) {
              match = f;
              best_flen = flen;
            }
          }
        }

        if (!match.empty()) {
          bool is_valid{true};
          if (m_verbosity >= Verbosity::INFO) {
            std::clog << "Reverse proxy server, filter '"
              << clients[match].filter << "' found at fd="
              << clients[match].fd << std::endl;
          }

          uint32_t data_in_length{0};
          if (data_in != nullptr) {
            data_in_length = data_in->length();
          }

          uint32_t subject_data_length = subject_data.length();

          uint32_t msg_length = data_in_length + subject_data_length + 21;
          for (auto field : fields) {
            msg_length += field.first.length() + field.second.length() + 8;
          }

          uint8_t buffer[TINYH2_BUFFER_SIZE];
          uint32_t req_id;

          if (msg_length > TINYH2_BUFFER_SIZE) {
            if (m_verbosity >= Verbosity::ON_ERROR) {
              std::clog << "Reverse proxy server, buffer overflow ("
                << TINYH2_BUFFER_SIZE << " < " << msg_length << ")" << std::endl;
            }
            std::lock_guard<std::mutex> const lock(clients_mutex);
            close_session(clients[match].fd);
            is_valid = false;
          } else {
            {
              std::lock_guard<std::mutex> const lock(global_req_count_mutex);
              req_id = global_req_count;
              global_req_count++;
            }

            uint32_t const msg_length_n = htonl(msg_length);
            uint32_t const req_id_n = htonl(req_id);
            uint32_t const fields_length_n = htonl(fields.size());
            uint32_t const data_in_length_n = htonl(data_in_length);
            uint32_t const subject_data_length_n = htonl(subject_data_length);

            uint32_t pos{0};

            buffer[0] = 0x01;
            ::memcpy(buffer + 1, &msg_length_n, 4);
            ::memcpy(buffer + 5, &req_id_n, 4);
            ::memcpy(buffer + 9, &fields_length_n, 4);
            ::memcpy(buffer + 13, &data_in_length_n, 4);
            ::memcpy(buffer + 17, &subject_data_length_n, 4);
            pos += 21;

            for (auto field : fields) {
              uint32_t const key_length = field.first.length();
              uint32_t const value_length = field.second.length();

              uint32_t const key_length_n{htonl(key_length)};
              uint32_t const value_length_n{htonl(value_length)};

              ::memcpy(buffer + pos, &key_length_n, 4);
              ::memcpy(buffer + pos + 4, field.first.data(), key_length);
              ::memcpy(buffer + pos + key_length + 4, &value_length_n, 4);
              ::memcpy(buffer + pos + key_length + 8, field.second.data(),
                  value_length);
              pos += key_length + value_length + 8;
            }

            if (data_in_length > 0) {
              ::memcpy(buffer + pos, data_in->data(), data_in_length);
              pos += data_in_length;
            }

            if (subject_data_length > 0) {
              ::memcpy(buffer + pos, subject_data.data(),
                  subject_data_length);
              pos += subject_data_length;
            }


            {
              int32_t r;
              {
                uint32_t buffer_length = pos;
                std::lock_guard<std::mutex> const lock(clients_mutex);
                r = ::send(clients[match].fd, buffer, buffer_length, 0);
              }
              if (r == -1 && errno == EPIPE) {
                std::lock_guard<std::mutex> const lock(clients_mutex);
                if (m_verbosity >= Verbosity::ON_ERROR) {
                  std::clog << "Reverse proxy server, client lost ("
                    << clients[match].fd << "), err=" << errno << std::endl;
                }
                close_session(clients[match].fd);
                is_valid = false;
              }
            }
          }

          std::string incoming;
          if (is_valid) {
            uint32_t d_n;

            bool is_peeking{true};
            uint32_t bytes_to_read{0};
            while (is_peeking || incoming.length() < bytes_to_read) {
              int32_t fd_copy;
              {
                std::lock_guard<std::mutex> const lock(clients_mutex);
                fd_copy = clients[match].fd;
              }

              int32_t r;
              {
                std::lock_guard<std::mutex> const lock(clients[match].mutex);
                if (is_peeking) {
                  r = ::recv(fd_copy, buffer, TINYH2_BUFFER_SIZE, MSG_PEEK);
                } else {
                  uint32_t n = std::min(
                      static_cast<int32_t>(bytes_to_read - incoming.length()),
                      TINYH2_BUFFER_SIZE);
                  r = ::recv(fd_copy, buffer, n, 0);
                }
              }

              if (r <= 0 && errno != 11) {
                if (errno == 0) {
                  if (m_verbosity >= Verbosity::INFO) {
                    std::clog << "Reverse proxy server, client left ("
                      << fd_copy << ")" << std::endl;
                  }
                } else {
                  if (m_verbosity >= Verbosity::ON_ERROR) {
                    std::clog << "Reverse proxy server, client lost ("
                      << fd_copy << "), err=" << errno << std::endl;
                  }
                }
                std::lock_guard<std::mutex> const lock(clients_mutex);
                close_session(clients[match].fd);
                is_valid = false;
                break;
              } else if (is_peeking && r >= 9) {
                ::memcpy(&d_n, buffer + 5, 4);
                uint32_t peeked_req_id = ntohl(d_n);
                if (peeked_req_id == req_id) {
                  ::memcpy(&d_n, buffer + 1, 4);
                  bytes_to_read = ntohl(d_n);
                  is_peeking = false;
                  continue;
                }
              }
              if (!is_peeking) {
                std::string str(r, ' ');
                ::memcpy(str.data(), buffer, r);
                incoming += str;
              }
            }
          }

          if (is_valid && (incoming.length() == 0 || incoming[0] != 2)) {
            if (m_verbosity >= Verbosity::ON_ERROR) {
              std::cerr << "Reverse proxy server, unexpected command ("
                << clients[match].fd << ")" << std::endl;
            }
            is_valid = false;
          }

          if (is_valid) {
            uint32_t d_n;
            uint32_t pos{0};
            ::memcpy(&d_n, incoming.data() + 9, 4);
            status_code = ntohl(d_n);
            ::memcpy(&d_n, incoming.data() + 13, 4);
            uint32_t content_type_length = ntohl(d_n);
            ::memcpy(&d_n, incoming.data() + 17, 4);
            uint32_t content_length = ntohl(d_n);
            pos += 21;

            std::string content_type_tmp(content_type_length, ' ');
            ::memcpy(content_type_tmp.data(), incoming.data() + pos,
                content_type_length);
            pos += content_type_length;
            content_type = content_type_tmp;

            std::string content_tmp(content_length, ' ');
            ::memcpy(content_tmp.data(), incoming.data() + pos,
                content_length);
            pos += content_length;
            content = content_tmp;
          }
        }

        std::shared_ptr<std::string const> content_ptr(
            new std::string(content));
        return {content_type, status_code, content_ptr};
    }};


  auto request_delegate{[this, &local_request_delegate, &request_message,
    &clients, &clients_mutex](
      std::map<std::string, std::string> fields,
      std::shared_ptr<std::string> data_in, std::string const &client_addr,
      std::function<void(std::string, uint32_t,
        std::shared_ptr<std::string const>)> send_response,
      std::function<void(std::string, void *, std::string const &)>) {

      std::vector<std::string> filter_names;
      {
        std::lock_guard<std::mutex> const lock(clients_mutex);
        for (auto &c : clients) {
          filter_names.push_back(c.second.filter);
        }
      }

      auto msg = local_request_delegate(fields, data_in, client_addr,
          filter_names, request_message);

      send_response(std::get<0>(msg), std::get<1>(msg), std::get<2>(msg));
    }};

  m_server.run(private_key_path, public_key_path, address_external,
      port_external, true, worker_count - 1, prepare_incoming_delegate,
      append_incoming_delegate, request_delegate);

  is_running.store(false);
  worker.join();

  ::shutdown(m_fd_listen, SHUT_RDWR);
  if (m_fd_listen != -1) {
    ::close(m_fd_listen);
  }

  return 0;
}

inline void SimpleReverseProxyServer::stop() noexcept {
  ::shutdown(m_fd_listen, SHUT_RDWR);
  if (m_fd_listen != -1) {
    ::close(m_fd_listen);
  }
}

class SimpleReverseProxyClient {
  public:
    SimpleReverseProxyClient(Verbosity = Verbosity::ON_ERROR) noexcept;
    SimpleReverseProxyClient(SimpleReverseProxyClient const &) = delete;
    SimpleReverseProxyClient(SimpleReverseProxyClient &&) = delete;
    ~SimpleReverseProxyClient() = default;

    SimpleReverseProxyClient &operator=(
        SimpleReverseProxyClient const &) = delete;
    SimpleReverseProxyClient &operator=(SimpleReverseProxyClient &&) = delete;

    int32_t connect(std::string, std::string, uint32_t,
        std::function<std::shared_ptr<std::string>(uint32_t)>,
        std::function<void(std::map<std::string, std::string>,
          std::shared_ptr<std::string>, std::shared_ptr<std::string>,
          std::function<void(std::string, std::string, uint32_t)>)>) noexcept;
    void stop() noexcept;

  private:
    std::mutex m_session_mutex;
    std::mutex m_recv_mutex;
    Verbosity m_verbosity;
    int32_t m_fd;
};

inline SimpleReverseProxyClient::SimpleReverseProxyClient(Verbosity verbosity)
  noexcept:
  m_session_mutex(),
  m_recv_mutex(),
  m_verbosity(verbosity),
  m_fd()
{
}

inline int32_t SimpleReverseProxyClient::connect(
    std::string filter, std::string address, uint32_t port,
    std::function<std::shared_ptr<std::string>(uint32_t)>
    prepare_incoming_delegate,
    std::function<void(std::map<std::string, std::string>,
      std::shared_ptr<std::string>, std::shared_ptr<std::string>,
      std::function<void(std::string, std::string, uint32_t)>
      )> request_delegate) noexcept
{
  struct sockaddr_in addr{};
  std::memset(&addr, 0, sizeof(addr));
  addr.sin_addr.s_addr = ::inet_addr(address.c_str());
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);

  bool has_socket{false};
  {
    std::lock_guard<std::mutex> const lock(m_session_mutex);
    m_fd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    has_socket = (m_fd >= 0);
  }

  std::vector<std::thread> workers;

  if (has_socket) {
    bool has_connection{false};
    {
      std::lock_guard<std::mutex> const lock(m_session_mutex);
      int32_t r = ::connect(m_fd, reinterpret_cast<struct sockaddr *>(&addr),
          sizeof(addr));
      has_connection = (r >= 0);
    }
    if (!has_connection) {
      if (m_verbosity >= Verbosity::ON_ERROR) {
        std::cerr << "Reverse proxy client, could not connect, err="
          << ::strerror(errno) << std::endl;
      }
      stop();
    } else {
      std::mutex mutex;

      uint32_t msg_length = filter.length() + 5;
      uint32_t msg_length_n{htonl(msg_length)};

      uint8_t buffer[TINYH2_BUFFER_SIZE];
      buffer[0] = 0x00;
      ::memcpy(buffer + 1, &msg_length_n, 4);
      ::memcpy(buffer + 5, filter.data(), filter.length());
      {
        std::lock_guard<std::mutex> const lock(m_session_mutex);
        ::send(m_fd, buffer, msg_length, 0);
      }

      while (true) {
        uint32_t d_n;

        bool is_peeking{true};
        uint32_t bytes_to_read{9};
        uint32_t req_id{0};
        std::string incoming;
        while (is_peeking || incoming.length() < bytes_to_read) {
          int32_t fd_copy;
          {
            std::lock_guard<std::mutex> const lock(m_session_mutex);
            fd_copy = m_fd;
          }
          int32_t r;
          if (is_peeking) {
            std::lock_guard<std::mutex> const lock(m_recv_mutex);
            r = ::recv(fd_copy, buffer, TINYH2_BUFFER_SIZE, MSG_PEEK);
          } else {
            uint32_t n = std::min(
                static_cast<int32_t>(bytes_to_read - incoming.length()),
                TINYH2_BUFFER_SIZE);
            {
              std::lock_guard<std::mutex> const lock(m_recv_mutex);
              r = ::recv(fd_copy, buffer, n, 0);
            }
          }

          if (r <= 0) {
            if (m_verbosity >= Verbosity::INFO) {
              std::clog << "Reverse proxy client, connection closed ("
                << fd_copy << ")" << std::endl;
            }
            stop();
            break;
          } else if (is_peeking && r >= 9) {
            ::memcpy(&d_n, buffer + 5, 4);
            req_id = ntohl(d_n);
            ::memcpy(&d_n, buffer + 1, 4);
            bytes_to_read = ntohl(d_n);
            is_peeking = false;
            continue;
          }
          if (!is_peeking) {
            std::string str(r, ' ');
            ::memcpy(str.data(), buffer, r);
            incoming += str;
          }
        }

        {
          std::lock_guard<std::mutex> const lock(m_session_mutex);
          if (m_fd == -1) {
            break;
          }
        }

        if (incoming[0] != 1) {
          if (m_verbosity >= Verbosity::ON_ERROR) {
            std::lock_guard<std::mutex> const lock(m_session_mutex);
            std::cerr << "Reverse proxy client, unexpected command ("
              << m_fd << ")" << std::endl;
          }
          continue;
        }

        uint32_t pos{0};
        ::memcpy(&d_n, incoming.data() + 9, 4);
        uint32_t fields_length = ntohl(d_n);
        ::memcpy(&d_n, incoming.data() + 13, 4);
        uint32_t data_in_length = ntohl(d_n);
        ::memcpy(&d_n, incoming.data() + 17, 4);
        uint32_t subject_data_length = ntohl(d_n);
        pos += 21;

        std::map<std::string, std::string> fields;
        std::shared_ptr<std::string> data_in;
        std::shared_ptr<std::string> subject_data;

        for (uint32_t i{0}; i < fields_length; ++i) {
          ::memcpy(&d_n, incoming.data() + pos, 4);
          pos += 4;
          uint32_t key_length = ntohl(d_n);

          std::string key(key_length, ' ');
          ::memcpy(key.data(), incoming.data() + pos, key_length);
          pos += key_length;

          ::memcpy(&d_n, incoming.data() + pos, 4);
          pos += 4;
          uint32_t value_length = ntohl(d_n);

          std::string value(value_length, ' ');
          ::memcpy(value.data(), incoming.data() + pos, value_length);
          pos += value_length;

          fields[key] = value;
        }

        if (data_in_length > 0) {
          data_in = prepare_incoming_delegate(data_in_length);
          ::memcpy(data_in->data(), incoming.data() + pos, data_in_length);
        }

        if (subject_data_length > 0) {
          subject_data = prepare_incoming_delegate(subject_data_length);
          ::memcpy(subject_data->data(), incoming.data() + pos,
              subject_data_length);
        }

        auto send_response{
          [this, &mutex, req_id](std::string const &content_type,
              std::string const &content, uint32_t status_code) {

            std::lock_guard<std::mutex> const lock0(mutex);

            uint32_t msg_len = content_type.length() + content.length() + 21;
            uint32_t msg_len_n{htonl(msg_len)};

            uint32_t req_id_n{htonl(req_id)};

            uint32_t status_code_n{htonl(status_code)};
            uint32_t content_type_length_n{htonl(content_type.length())};
            uint32_t content_length_n{htonl(content.length())};

            std::string buf(msg_len, ' ');

            buf[0] = 0x02;
            ::memcpy(buf.data() + 1, &msg_len_n, 4);
            ::memcpy(buf.data() + 5, &req_id_n, 4);
            ::memcpy(buf.data() + 9, &status_code_n, 4);
            ::memcpy(buf.data() + 13, &content_type_length_n, 4);
            ::memcpy(buf.data() + 17, &content_length_n, 4);
            ::memcpy(buf.data() + 21, content_type.data(),
                content_type.length());
            ::memcpy(buf.data() + content_type.length() + 21, content.data(),
                content.length());

            {
              std::lock_guard<std::mutex> const lock1(m_session_mutex);
              ::send(m_fd, buf.data(), msg_len, 0);
            }
          }};

        workers.emplace_back(request_delegate, fields, data_in, subject_data,
            send_response);
      }
    }
  }
  for (auto &w : workers) {
    w.join();
  }
  return 0;
}

inline void SimpleReverseProxyClient::stop() noexcept {
  std::lock_guard<std::mutex> const lock0(m_session_mutex);
  ::shutdown(m_fd, SHUT_RDWR);
  std::lock_guard<std::mutex> const lock1(m_recv_mutex);
  if (m_fd != -1) {
    ::close(m_fd);
  }
  m_fd = -1;
}

}

#endif
